const plugin = require('tailwindcss/plugin');

const addWHUtilities = plugin(function ({ addUtilities, e }) {
	const newUtilities = {};

	for(let i = 0; i <= 100; i += 5) {
		newUtilities[`.max-w-${i}-vw`] = {
			'max-width': `${i}vw`
		};

		newUtilities[`.max-h-${i}-vh`] = {
			'max-height': `${i}vh`
		};
	}

	newUtilities[`.${e('max-w-1/3-vw')}`] = {
		'max-width': '33.33vw'
	};

	newUtilities[`.${e('max-h-1/3-vh')}`] = {
		'max-height': '33.33vh'
	};

	newUtilities[`.${e('max-h-2/3-vh')}`] = {
		'max-height': '66.67vh'
	};

	addUtilities(newUtilities, ['responsive', 'hover']);
});

module.exports = addWHUtilities;
