const plugin = require('tailwindcss/plugin');

const addFlexUtilities = plugin(function ({ addUtilities, e }) {
	const newUtilities = {};

	['.fw-', '.fh-'].forEach((flexDir, idx) => {
		// eslint-disable-next-line curly
		for(let i = 0; i <= 100; i += 5) {
			newUtilities[`${flexDir}${i}`] = {
				'flex': `1 1 100%`,
				'max-height': (idx ? `${i}%` : '100%'),
				'max-width': (idx ? '100%' : `${i}%`)
			};
		}
	});

	newUtilities[`.${e('fw-1/3')}`] = {
		'flex': `1 1 100%`,
		'max-height': '100%',
		'max-width': '33.33%'
	};

	newUtilities[`.${e('fw-2/3')}`] = {
		'flex': `1 1 100%`,
		'max-height': '100%',
		'max-width': '66.67%'
	};

	newUtilities[`.${e('fh-1/3')}`] = {
		'flex': `1 1 100%`,
		'max-height': '33.33%',
		'max-width': '100%'
	};

	newUtilities[`.${e('fh-2/3')}`] = {
		'flex': `1 1 100%`,
		'max-height': '66.67%',
		'max-width': '100%'
	};

	newUtilities[`.fw-unset`] = {
		'flex': `initial`,
		'max-height': 'none',
		'max-width': 'none'
	};

	newUtilities[`.fh-unset`] = {
		'flex': `initial`,
		'max-height': 'none',
		'max-width': 'none'
	};

	addUtilities(newUtilities, ['responsive', 'hover']);
});

module.exports = addFlexUtilities;
