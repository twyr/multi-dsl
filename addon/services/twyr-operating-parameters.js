import Service from '@ember/service';
import debugLogger from 'ember-debug-logger';

import { tracked } from 'tracked-built-ins';

export default class TwyrOperatingParametersService extends Service {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	browser = tracked({});
	cpu = tracked({});
	device = tracked({});
	engine = tracked({});
	os = tracked({});
	elemDesignSystem = tracked(Map);
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);

		this?._refreshOperatingParameters?.();
		this.#checkInterval = setInterval(this?._refreshOperatingParameters?.bind?.(this), 500);
	}
	// #endregion

	// #region Lifecycle Hooks
	willDestroy() {
		if(this.#checkInterval) {
			clearInterval(this.#checkInterval);
			this.#checkInterval = null;
		}

		this.#debug(`willDestroy`);
		super.willDestroy(...arguments);
	}
	// #endregion

	// #region Computed Properties
	// #endregion

	// #region Public Methods
	watchDesignSystem(element, callback) {
		this.#debug(`watchDesignSystem: `, element);

		const elementStyle = getComputedStyle(element);
		const designSystem = elementStyle?.getPropertyValue?.('--twyr-design-system');

		this?.elemDesignSystem?.set?.(element, {
			'designSystem': designSystem?.trim?.(),
			'callback': callback
		});

		callback?.(designSystem?.trim?.());
	}

	unwatchDesignSystem(element) {
		this.#debug(`unwatchDesignSystem: `, element);
		this?.elemDesignSystem?.delete?.(element);
	}
	// #endregion

	// #region Private Methods
	_refreshOperatingParameters() {
		if(this.#isCheckRunning)
			return;

		this.#isCheckRunning = true;

		// Refresh all the design system map values
		this?.elemDesignSystem?.forEach?.((value, key) => {
			// this.#debug(`checking design system for ${key.nodeName}, with current value: ${value}`);

			const elementStyle = window?.getComputedStyle?.(key);
			const designSystem = elementStyle?.getPropertyValue?.('--twyr-design-system')?.trim?.();

			// eslint-disable-next-line curly
			if(value?.designSystem === designSystem) {
				// this.#debug(`design system for ${key.nodeName} has not changed - not notifying`);
				return;
			}

			this.#debug(`setting design system for ${key?.nodeName}, with new value: ${designSystem}`);

			const elementRecord = this?.elemDesignSystem?.get?.(key);
			elementRecord['designSystem'] = designSystem;
			elementRecord?.callback?.(designSystem);
		});

		// If the user-agent has changed....
		// eslint-disable-next-line curly
		if(window.navigator.userAgent === this.#currentUA) {
			// this.#debug(`refreshOperatingParameters: user-agent has not changed - not notifying`);

			this.#isCheckRunning = false;
			return;
		}

		this.#currentUA = window.navigator.userAgent;

		// eslint-disable-next-line new-cap
		const currentUA = window?.UAParser?.();

		this.#debug(`refreshOperatingParameters::user-agent has changed::notifying: `, currentUA);
		this.browser.name = currentUA?.browser?.name;
		this.browser.version = currentUA?.browser?.version;

		this.cpu.architecture = currentUA?.cpu?.architecture;

		this.device.model = currentUA?.device?.model;
		this.device.type = currentUA?.device?.type;
		this.device.vendor = currentUA?.device?.vendor;

		this.engine.name = currentUA?.engine?.name;
		this.engine.version = currentUA?.engine?.version;

		this.os.name = currentUA?.os?.name;
		this.os.version = currentUA?.os?.version;

		this.#isCheckRunning = false;
	}
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('service:twyr-operating-parameters');
	#checkInterval = null;
	#currentUA = null;
	#isCheckRunning = false;
	// #endregion
}
