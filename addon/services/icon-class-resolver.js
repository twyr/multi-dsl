import Service from '@ember/service';
import debugLogger from 'ember-debug-logger';

export default class IconClassResolverService extends Service {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	// #endregion

	// #region Computed Properties
	// #endregion

	// #region Public Methods
	getIconClass(icon, designSystem) {
		designSystem = designSystem?.trim?.() ?? 'tailwind';
		const iconClass = (this.#iconMapping?.[icon]?.[designSystem]) ?? (this.#iconMapping?.[icon]?.['default']);

		this.#debug(`getIconClass: ${icon}, ${designSystem}, iconClass: ${iconClass}`);
		return iconClass;
	}
	// #endregion

	// #region Private Methods
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('service:icon-class-resolver');

	#iconMapping = {
		'align-bottom': {
			'bootstrap': 'bi bi-align-bottom',
			'material': 'mdi mdi-align-vertical-bottom',
			'patternfly': 'bi bi-align-bottom',
			'tailwind': 'ri-align-bottom'
		},
		'align-middle': {
			'bootstrap': 'bi bi-align-middle',
			'material': 'mdi mdi-align-vertical-center',
			'patternfly': 'bi bi-align-middle',
			'tailwind': 'ri-align-vertically'
		},
		'align-top': {
			'bootstrap': 'bi bi-align-top',
			'material': 'mdi mdi-format-align-vertical-top',
			'patternfly': 'bi bi-align-top',
			'tailwind': 'ri-align-top'
		},

		'align-end': {
			'bootstrap': 'bi bi-align-end',
			'material': 'mdi mdi-align-horizontal-right',
			'patternfly': 'fas fa-align-right',
			'tailwind': 'ri-align-right'
		},
		'align-center': {
			'bootstrap': 'bi bi-align-center',
			'material': 'mdi mdi-align-horizontal-center',
			'patternfly': 'fas fa-align-center',
			'tailwind': 'ri-align-center'
		},
		'align-start': {
			'bootstrap': 'bi bi-align-start',
			'material': 'mdi mdi-format-align-horizontal-left',
			'patternfly': 'bi bi-align-start',
			'tailwind': 'ri-align-left'
		},

		'arrow-bar-down': {
			'bootstrap': 'bi bi-arrow-bar-down',
			'material': 'mdi mdi-arrow-expand-down',
			'patternfly': 'bi bi-arrow-bar-down',
			'tailwind': 'ri-arrow-down-fill'
		},
		'arrow-bar-left': {
			'bootstrap': 'bi bi-arrow-bar-left',
			'material': 'mdi mdi-arrow-expand-left',
			'patternfly': 'bi bi-arrow-bar-left',
			'tailwind': 'ri-arrow-left-fill'
		},
		'arrow-bar-right': {
			'bootstrap': 'bi bi-arrow-bar-right',
			'material': 'mdi mdi-arrow-expand-right',
			'patternfly': 'bi bi-arrow-bar-right',
			'tailwind': 'ri-arrow-right-fill'
		},
		'arrow-bar-up': {
			'bootstrap': 'bi bi-arrow-bar-up',
			'material': 'mdi mdi-arrow-expand-up',
			'patternfly': 'bi bi-arrow-bar-up',
			'tailwind': 'ri-arrow-up-fill'
		},

		'arrow-down': {
			'bootstrap': 'bi bi-arrow-down',
			'material': 'mdi mdi-arrow-down',
			'patternfly': 'fas fa-arrow-down',
			'tailwind': 'ri-arrow-down-line'
		},
		'arrow-left': {
			'bootstrap': 'bi bi-arrow-left',
			'material': 'mdi mdi-arrow-left',
			'patternfly': 'fas fa-arrow-left',
			'tailwind': 'ri-arrow-left-line'
		},
		'arrow-right': {
			'bootstrap': 'bi bi-arrow-right',
			'material': 'mdi mdi-arrow-right',
			'patternfly': 'fas fa-arrow-right',
			'tailwind': 'ri-arrow-right-line'
		},
		'arrow-up': {
			'bootstrap': 'bi bi-arrow-up',
			'material': 'mdi mdi-arrow-up',
			'patternfly': 'fas fa-arrow-up',
			'tailwind': 'ri-arrow-up-line'
		},

		'arrow-down-left': {
			'bootstrap': 'bi bi-arrow-down-left',
			'material': 'mdi mdi-arrow-bottom-left',
			'patternfly': 'bi bi-arrow-down-left',
			'tailwind': 'ri-arrow-left-down-line'
		},
		'arrow-down-right': {
			'bootstrap': 'bi bi-arrow-down-right',
			'material': 'mdi mdi-arrow-bottom-right',
			'patternfly': 'bi bi-arrow-down-right',
			'tailwind': 'ri-arrow-right-down-line'
		},
		'arrow-up-left': {
			'bootstrap': 'bi bi-arrow-up-left',
			'material': 'mdi mdi-arrow-top-left',
			'patternfly': 'bi bi-arrow-up-left',
			'tailwind': 'ri-arrow-left-up-line'
		},
		'arrow-up-right': {
			'bootstrap': 'bi bi-arrow-up-right',
			'material': 'mdi mdi-arrow-top-right',
			'patternfly': 'bi bi-arrow-up-right',
			'tailwind': 'ri-arrow-right-up-line'
		},

		'arrow-up-down': {
			'bootstrap': 'bi bi-arrow-down-up',
			'material': 'mdi mdi-arrow-up-down',
			'patternfly': 'bi bi-arrow-down-up',
			'tailwind': 'ri-arrow-up-down-line'
		},
		'arrow-left-right': {
			'bootstrap': 'bi bi-arrow-left-right',
			'material': 'mdi mdi-arrow-left-right',
			'patternfly': 'fas fa-exchange-alt',
			'tailwind': 'ri-arrow-left-right-line'
		},

		'arrows-angle-contract': {
			'bootstrap': 'bi bi-arrows-angle-contract',
			'material': 'mdi mdi-arrow-collpase',
			'patternfly': 'fas fa-compress-alt',
			'tailwind': 'bi bi-arrows-angle-contract'
		},
		'arrows-angle-expand': {
			'bootstrap': 'bi bi-arrows-angle-expand',
			'material': 'mdi mdi-arrow-expand',
			'patternfly': 'fas fa-expand-alt',
			'tailwind': 'bi bi-arrows-angle-expand'
		},

		'arrows-collapse': {
			'bootstrap': 'bi bi-arrows-collapse',
			'material': 'mdi mdi-arrow-collapse-vertical',
			'patternfly': 'bi bi-arrows-collapse',
			'tailwind': 'bi bi-arrows-collapse'
		},
		'arrows-expand': {
			'bootstrap': 'bi bi-arrows-expand',
			'material': 'mdi mdi-arrow-expand-vertical',
			'patternfly': 'bi bi-arrows-expand',
			'tailwind': 'bi bi-arrows-expand'
		},

		'arrows-fullscreen': {
			'bootstrap': 'bi bi-arrows-fullscreen',
			'material': 'mdi mdi-arrow-expand-all',
			'patternfly': 'fas fa-expand-arrows-alt',
			'tailwind': 'ri-fullscreen-line'
		},
		'arrows-move': {
			'bootstrap': 'bi bi-arrows-move',
			'material': 'mdi mdi-arrow-all',
			'patternfly': 'fas fa-arrows-alt',
			'tailwind': 'ri-drag-move-line'
		},

		'angle-down': {
			'bootstrap': 'bi bi-chevron-down',
			'material': 'mdi mdi-chevron-down',
			'patternfly': 'fas fa-angle-down',
			'tailwind': 'ri-arrow-down-s-line'
		},
		'caret-down': {
			'bootstrap': 'bi bi-caret-down-fill',
			'material': 'mdi mdi-menu-down',
			'patternfly': 'fas fa-caret-down',
			'tailwind': 'ri-arrow-down-s-fill'
		},
		'angle-left': {
			'bootstrap': 'bi bi-chevron-left',
			'material': 'mdi mdi-chevron-left',
			'patternfly': 'fas fa-angle-left',
			'tailwind': 'ri-arrow-left-s-line'
		},
		'caret-left': {
			'bootstrap': 'bi bi-caret-left-fill',
			'material': 'mdi mdi-menu-left',
			'patternfly': 'fas fa-caret-left',
			'tailwind': 'ri-arrow-left-s-fill'
		},
		'angle-right': {
			'bootstrap': 'bi bi-chevron-right',
			'material': 'mdi mdi-chevron-right',
			'patternfly': 'fas fa-angle-right',
			'tailwind': 'ri-arrow-right-s-line'
		},
		'caret-right': {
			'bootstrap': 'bi bi-caret-right-fill',
			'material': 'mdi mdi-menu-right',
			'patternfly': 'fas fa-caret-right',
			'tailwind': 'ri-arrow-right-s-fill'
		},
		'angle-up': {
			'bootstrap': 'bi bi-chevron-up',
			'material': 'mdi mdi-chevron-up',
			'patternfly': 'fas fa-angle-up',
			'tailwind': 'ri-arrow-up-s-line'
		},
		'caret-up': {
			'bootstrap': 'bi bi-caret-up-fill',
			'material': 'mdi mdi-menu-up',
			'patternfly': 'fas fa-caret-up',
			'tailwind': 'ri-arrow-up-s-fill'
		},

		'justify': {
			'bootstrap': 'bi bi-justify',
			'material': 'mdi mdi-format-align-justify',
			'patternfly': 'fas fa-align-justify',
			'tailwind': 'ri-align-justify'
		},
		'justify-left': {
			'bootstrap': 'bi bi-justify-left',
			'material': 'mdi mdi-format-align-left',
			'patternfly': 'fas fa-align-left',
			'tailwind': 'ri-align-left'
		},
		'justify-right': {
			'bootstrap': 'bi bi-justify-right',
			'material': 'mdi mdi-format-align-right',
			'patternfly': 'fas fa-align-right',
			'tailwind': 'ri-align-right'
		},
		'justify-center': {
			'bootstrap': 'bi bi-text-center',
			'material': 'mdi mdi-format-align-center',
			'patternfly': 'fas fa-align-center',
			'tailwind': 'ri-align-center'
		},

		'home': {
			'bootstrap': 'bi bi-house',
			'material': 'mdi mdi-home',
			'patternfly': 'fas fa-home',
			'tailwind': 'ri-home-line'
		},

		'alarm-empty': {
			'bootstrap': 'bi bi-alarm',
			'material': 'mdi mdi-alarm',
			'patternfly': 'far fa-bell',
			'tailwind': 'ri-notification-line'
		},
		'alarm-fill': {
			'bootstrap': 'bi bi-alarm-fill',
			'material': 'mdi mdi-alarm-bell',
			'patternfly': 'fas fa-bell',
			'tailwind': 'ri-notification-fill'
		},

		'asterisk': {
			'bootstrap': 'bi bi-asterisk',
			'material': 'mdi mdi-asterisk',
			'patternfly': 'fas fa-asterisk',
			'tailwind': 'ri-asterisk'
		},

		'check': {
			'bootstrap': 'bi bi-check2',
			'material': 'mdi mdi-check',
			'patternfly': 'fas fa-check',
			'tailwind': 'ri-check-line'
		},
		'check-all': {
			'bootstrap': 'bi bi-check2-all',
			'material': 'mdi mdi-check-all',
			'patternfly': 'fas fa-check-double',
			'tailwind': 'ri-check-double-line'
		},

		'dash-large': {
			'bootstrap': 'bi bi-dash-lg',
			'material': 'mdi mdi-minus-thick',
			'patternfly': 'fas fa-minus',
			'tailwind': 'ri-subtract-fill'
		},

		'redo': {
			'bootstrap': 'bi bi-arrow-clockwise',
			'material': 'mdi mdi-rotate-right',
			'patternfly': 'fas fa-redo',
			'tailwind': 'ri-arrow-go-forward-line'
		},
		'undo': {
			'bootstrap': 'bi bi-arrow-counterclockwise',
			'material': 'mdi mdi-rotate-left',
			'patternfly': 'fas fa-undo',
			'tailwind': 'ri-arrow-go-back-line'
		},

		'refresh': {
			'bootstrap': 'bi bi-arrow-repeat',
			'material': 'mdi mdi-sync',
			'patternfly': 'fas fa-sync',
			'tailwind': 'ri-refresh-line'
		},

		'download': {
			'bootstrap': 'bi bi-download',
			'material': 'mdi mdi-download',
			'patternfly': 'fas fa-download',
			'tailwind': 'ri-download-line'
		},
		'upload': {
			'bootstrap': 'bi bi-upload',
			'material': 'mdi mdi-upload',
			'patternfly': 'fas fa-upload',
			'tailwind': 'ri-upload-line'
		},

		'hamburger-menu': {
			'bootstrap': 'bi bi-list',
			'material': 'mdi mdi-menu',
			'patternfly': 'fas fa-bars',
			'tailwind': 'ri-menu-line'
		},
		'overflow-menu': {
			'bootstrap': 'bi bi-three-dots-vertical',
			'material': 'mdi mdi-dots-vertical',
			'patternfly': 'fas fa-ellipsis-v',
			'tailwind': 'ri-more-2-line'
		},
		'trash': {
			'bootstrap': 'bi bi-trash',
			'material': 'mdi mdi-trash-can-outline',
			'patternfly': 'far fa-trash-alt',
			'tailwind': 'ri-delete-bin-line'
		},

		'plus': {
			'bootstrap': 'bi bi-plus',
			'material': 'mdi mdi-plus',
			'patternfly': 'fas fa-plus',
			'tailwind': 'ri-add-line'
		},
		'plus-circle': {
			'bootstrap': 'bi bi-plus-circle',
			'material': 'mdi mdi-plus-circle-outline',
			'patternfly': 'fas fa-plus-circle',
			'tailwind': 'ri-add-circle-line'
		},
		'plus-circle-fill': {
			'bootstrap': 'bi bi-plus-circle-fill',
			'material': 'mdi mdi-plus-circle',
			'patternfly': 'fas fa-plus-circle',
			'tailwind': 'ri-add-circle-fill'
		},
		'plus-square': {
			'bootstrap': 'bi bi-plus-square',
			'material': 'mdi mdi-plus-box-outline',
			'patternfly': 'far fa-plus-square',
			'tailwind': 'ri-add-box'
		},
		'plus-square-fill': {
			'bootstrap': 'bi bi-plus-square-fill',
			'material': 'mdi mdi-plus-box',
			'patternfly': 'fas fa-plus-square',
			'tailwind': 'ri-add-box-fill'
		},

		'minus': {
			'bootstrap': 'bi bi-dash',
			'material': 'mdi mdi-minus',
			'patternfly': 'fas fa-minus',
			'tailwind': 'ri-subtract-line'
		},
		'minus-circle': {
			'bootstrap': 'bi bi-dash-circle',
			'material': 'mdi mdi-minus-circle-outline',
			'patternfly': 'fas fa-minus-circle',
			'tailwind': 'bi bi-dash-circle'
		},
		'minus-circle-fill': {
			'bootstrap': 'bi bi-dash-circle-fill',
			'material': 'mdi mdi-minus-circle',
			'patternfly': 'fas fa-minus-circle',
			'tailwind': 'bi bi-dash-circle-fill'
		},
		'minus-square': {
			'bootstrap': 'bi bi-dash-square',
			'material': 'mdi mdi-minus-box-outline',
			'patternfly': 'far fa-minus-square',
			'tailwind': 'bi bi-dash-square'
		},
		'minus-square-fill': {
			'bootstrap': 'bi bi-dash-square-fill',
			'material': 'mdi mdi-minus-box',
			'patternfly': 'fas fa-minus-square',
			'tailwind': 'bi bi-dash-square-fill'
		},

		'close': {
			'bootstrap': 'bi bi-x',
			'material': 'mdi mdi-close',
			'patternfly': 'fas fa-times',
			'tailwind': 'ri-close-line'
		}
	}
	// #endregion
}
