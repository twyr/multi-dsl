import Service from '@ember/service';
import debugLogger from 'ember-debug-logger';

import { inject as service } from '@ember/service';
import { v4 as uuidv4 } from 'uuid';

export default class AlertManagerService extends Service {
	// #region Accessed Services
	@service('colourUtilities') colourUtilities;
	@service('iconClassResolver') iconClassResolver;
	// #endregion

	// #region Tracked Attributes
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	willDestroy() {
		this.#debug(`willDestroy`);
		this.#currentAlerts?.forEach?.((alertOptions) => {
			this?._closeAlert?.(alertOptions?.id);
		});

		super.willDestroy(...arguments);
	}
	// #endregion

	// #region Computed Properties
	// #endregion

	// #region Public Methods
	notify(options) {
		this.#debug(`notify: `, options);

		// Create the Alert Manager Element if required
		if(!this.#currentAlerts?.length) this._createAlertManagerElement();

		// Create the Alert Element itself
		const thisAlertOptions = Object?.assign?.({}, this.#defaultOptions, options);
		thisAlertOptions.id = uuidv4();
		thisAlertOptions.expiry = (thisAlertOptions?.timeout >= 0) ? Date?.now?.() + thisAlertOptions?.timeout : -1;

		const alertElement = this?._createAlertElement?.(thisAlertOptions);

		// Add the Alert Element into the DOM
		let alertDisplayElement = this.#alertManagerElement?.querySelector?.(`div.twyr-alert-area.${thisAlertOptions.xPosition}.${thisAlertOptions.yPosition}`);
		if(!alertDisplayElement) alertDisplayElement = this.#alertManagerElement?.querySelector?.('div.twyr-alert-area.right.bottom');

		this.#currentAlerts?.push?.(thisAlertOptions);
		alertDisplayElement?.append?.(alertElement);

		// Setup monitoring...
		// eslint-disable-next-line curly
		if(this.#currentAlerts?.length === 1) {
			this.#cleanUpInterval = setInterval?.(this?._cleanUpAlerts?.bind?.(this), 250);
		}
	}
	// #endregion

	// #region Private Methods
	_createAlertManagerElement() {
		if(this.#alertManagerElement)
			return;

		this.#alertManagerElement = document.createElement('div');
		this.#alertManagerElement.classList.add('twyr-alert-manager');

		['left', 'center', 'right'].forEach((xPosition) => {
			['top', 'middle', 'bottom'].forEach((yPosition) => {
				const alertArea = document.createElement('div');
				alertArea.classList.add('twyr-alert-area', xPosition, yPosition);

				this.#alertManagerElement?.append?.(alertArea);
			});
		});

		document?.body?.append?.(this.#alertManagerElement);
	}

	_createAlertElement(options) {
		const alertElement = document?.createElement?.('div');
		alertElement?.classList?.add?.('twyr-alert', options?.type);
		alertElement?.setAttribute?.('id', options?.id);

		if(options?.customClass) {
			if(!Array.isArray(options?.customClass))
				options.customClass = options?.customClass
					?.toString?.()
					?.split?.(' ')
					?.map?.((aClass) => { return aClass?.trim?.(); })
					?.filter?.((aClass) => { return !!aClass; });

			options?.alertClass?.forEach?.((aClass) => {
				alertElement?.classList?.add?.(aClass);
			});
		}

		// eslint-disable-next-line curly
		if(options?.customStyle) {
			alertElement.style.cssText = options?.customStyle;
		}

		if(options?.icon === true) {
			let alertIcon = null;
			switch (options?.type) {
				case 'success':
					alertIcon = 'check';
					break;

				case 'info':
					alertIcon = 'asterisk';
					break;

				case 'warning':
					alertIcon = 'asterisk';
					break;

				case 'danger':
					alertIcon = 'asterisk';
					break;

				default:
					alertIcon = 'alarm-fill';
					break;
			}

			const elementStyle = window?.getComputedStyle?.(this.#alertManagerElement);
			const designSystem = elementStyle?.getPropertyValue?.('--twyr-design-system')?.trim?.();

			let iconClasses = this?.iconClassResolver?.getIconClass?.(alertIcon, designSystem);
			iconClasses = iconClasses?.split?.(' ')?.map?.((iconClass) => { return iconClass?.trim?.(); })?.filter?.((iconClass) => { return !!iconClass; });

			const iconElement = document?.createElement?.('i');
			iconElement?.classList?.add?.('mr-2');
			iconClasses?.forEach?.((iconClass) => {
				iconElement?.classList?.add?.(iconClass);
			});

			alertElement?.append?.(iconElement);
		}

		const textElement = document?.createElement?.('span');
		textElement.innerText = options?.text;
		alertElement?.append?.(textElement);

		if(options?.closeOnClick) {
			alertElement?.classList?.add?.('cursor-pointer');
			alertElement?.addEventListener?.('click', this?._closeAlert?.bind?.(this, options?.id));
		}

		return alertElement;
	}

	_cleanUpAlerts() {
		const currentTime = Date?.now?.();

		let removeAlertIds = [];
		this.#currentAlerts?.forEach?.((alertOptions) => {
			if(alertOptions?.expiry < 0)
				return;

			if(alertOptions?.expiry < currentTime)
				removeAlertIds?.push?.(alertOptions?.id);
		});

		removeAlertIds = removeAlertIds.filter((alertId) => { return !!alertId; });
		removeAlertIds.forEach((alertId) => { this._closeAlert(alertId); });

		if(!this.#currentAlerts.length) {
			clearInterval(this.#cleanUpInterval);

			document?.body?.removeChild?.(this.#alertManagerElement);
			this.#alertManagerElement?.remove?.();
			this.#alertManagerElement = null;
		}
	}

	_closeAlert(alertId) {
		const alertElement = this.#alertManagerElement?.querySelector?.(`div[id = "${alertId}"]`);
		alertElement?.removeEventListener?.('click', this?._closeAlert?.bind?.(this, alertId));
		alertElement?.remove?.();

		let currentAlertIdx = -1;
		for(let idx = 0; idx < this.#currentAlerts?.length; idx++) {
			const currentAlert = this.#currentAlerts?.[idx];
			if(currentAlert?.id !== alertId) continue;

			currentAlertIdx = idx;
			break;
		}

		if(currentAlertIdx < 0)
			return;

		this.#currentAlerts.splice(currentAlertIdx, 1);
	}
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('service:alert-manager');

	#alertManagerElement = null;
	#currentAlerts = [];
	#cleanUpInterval = null;

	#defaultOptions = {
		'type': 'default',
		'timeout': 3000,
		'xPosition': 'right',
		'yPosition': 'bottom',
		'icon': true,
		'text': 'Hello, World',
		'closeOnClick': true
	}
	// #endregion
}
