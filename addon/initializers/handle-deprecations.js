/* eslint-disable func-style */
import { registerDeprecationHandler } from '@ember/debug';

export function initialize() {
	registerDeprecationHandler((message, options, next) => {
		if(options?.until === '4.0.0')
			return;

		next(message, options);
	});
}

export default { initialize };
