/* eslint-disable curly */
import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class TwyrButtonComponent extends Component {
	// #region Accessed Services
	@service('colourUtilities') colourUtilities;
	// #endregion

	// #region Tracked Attributes
	@tracked designSystem = null;
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	// #endregion

	// #region DOM Event Handlers
	@action
	onClassListMutation(entries) {
		this.#debug(`onClassListMutation: `, JSON.stringify(entries, null, '\t'));

		if(this?.designSystem === 'bootstrap') {
			this.#debug(`onClassListMutation::designSystem: bootstrap. setting up border ring colour..`);
			this?._setupBorderRingColour?.();
		}

		if(this?.designSystem === 'material') {
			this.#debug(`onClassListMutation::designSystem: material. aborting...`);
			return;
		}

		if(!this.#overlayElement) {
			this.#debug(`onClassListMutation: no overlay element. aborting...`);
			return;
		}

		const overlayColour = this?.colourUtilities?.getOverlayColour?.(this.#element);
		if(this.#overlayElement?.style?.borderColor === overlayColour) {
			this.#debug(`onClassListMutation: overlay element looks fine. aborting...`);
			return;
		}

		this.#overlayElement.style.backgroundColor = overlayColour;
		this.#overlayElement.style.borderColor = overlayColour;
	}

	@action
	onDesignSystemChange(designSystem) {
		this.#debug(`onDesignSystemChange::designSystem: ${designSystem}`);
		this.designSystem = designSystem;
	}

	@action
	onFocusIn() {
		this.#debug(`onFocusIn`);
		this?.args?.onFocusIn?.(...arguments);

		if(this.designSystem === 'bootstrap') {
			this.#debug(`onFocusIn: design system is "bootstrap" - setting up border ring...`);
			this?._setupBorderRingColour?.();
		}
	}

	@action
	onFocusOut() {
		this.#debug(`onFocusOut`);
		this?.args?.onFocusOut?.(...arguments);

		if(!this.#overlayElement)
			return;

		this.#debug(`onMouseLeave`);
		this.#element?.removeChild?.(this.#overlayElement);
		this.#overlayElement?.remove?.();
		this.#overlayElement = null;
	}

	@action
	onMouseEnter() {
		this.#debug(`onMouseEnter`);
		this?.args?.onMouseEnter?.(...arguments);

		if(this.designSystem === 'material') {
			this.#debug(`onMouseEnter: design system is "material" - aborting...`);
			return;
		}

		const buttonClasses = Array?.from?.(this.#element?.classList);
		let appliedClass = null;

		while(buttonClasses?.length) {
			appliedClass = buttonClasses?.pop?.();
			if(['text-button', 'outline-button', 'filled-button']?.includes?.(appliedClass))
				break;

			appliedClass = null;
		}

		if(appliedClass === 'outline-button') {
			this.#debug(`onMouseEnter: appliedClass is "outline-button" - aborting...`);
			return;
		}

		this?._setupOverlay?.();
		this.#debug(`onMouseEnter::applied the overlay`);
	}

	@action
	onMouseLeave() {
		this?.args?.onMouseLeave?.(...arguments);

		if(this.designSystem === 'material')
			return;

		if(!this.#overlayElement)
			return;

		this.#debug(`onMouseLeave`);
		this.#element?.removeChild?.(this.#overlayElement);
		this.#overlayElement?.remove?.();
		this.#overlayElement = null;
	}

	@action
	storeElement(element) {
		this.#debug(`storeElement: `, element);
		this.#element = element;
	}
	// #endregion

	// #region Computed Properties
	get stopPropagation() {
		return this?.args?.stopPropagation ?? true;
	}
	// #endregion

	// #region Private Methods
	_setupBorderRingColour() {
		const elementStyle = window?.getComputedStyle?.(this.#element);
		let ringColour = elementStyle?.['backgroundColor'];
		let opacity = 1;

		this.#debug(`_setupBorderRingColour::current state: ring color [background-color]: ${ringColour}, opacity: ${opacity}`);
		if(ringColour.indexOf('rgba(') === 0) {
			opacity = ringColour?.replace?.('rgba(', '')?.replace?.(')', '')?.split?.(',')?.pop?.()?.trim?.();
		}

		if(Number(opacity) === 0) {
			ringColour = elementStyle?.['color'];
			this.#debug(`_setupBorderRingColour::current state: ring color [text-color]: ${ringColour}, opacity: ${opacity}`);
		}

		opacity = elementStyle?.getPropertyValue?.('--tw-ring-opacity');

		this.#debug(`_setupBorderRingColour::pre-calculated state: ring color: ${ringColour}, opacity: ${opacity}`);
		ringColour = this?.colourUtilities?.parseColour?.(ringColour, opacity);
		this.#debug(`_setupBorderRingColour::post-calculated state: ring color: ${ringColour?.toString?.()?.trim?.()}`);

		const currentRingColour = this.#element?.style?.getPropertyValue?.('--tw-ring-color');
		if(currentRingColour === ringColour) {
			this.#debug(`_setupBorderRingColour::ring color hasn't changed. aborting...`);
			return;
		}

		this.#element?.style?.setProperty?.('--tw-ring-color', ringColour);
		this.#debug(`_setupBorderRingColour::new state::ring color: ${elementStyle?.getPropertyValue?.('--tw-ring-color')}`);
	}

	_setupOverlay() {
		const overlayColour = this?.colourUtilities?.getOverlayColour?.(this.#element);
		this.#debug(`_setupOverlay::overlayColour: `, overlayColour);

		const overlay = this.#overlayElement ?? document?.createElement?.('div');
		overlay?.classList?.add?.('twyr-button-overlay');

		const width = this.#element.clientWidth;
		const height = this.#element.clientHeight;

		const overlayCss = `
			position:absolute;
			left: 0px;
			top: 0px;
			width: ${width}px;
			height: ${height}px;
			background-color: ${overlayColour};
			border-color: ${overlayColour};
			pointer-events: none;
		`;

		overlay.style.cssText = overlayCss;

		this.#overlayElement = overlay;
		this.#element?.appendChild?.(this.#overlayElement);
	}
	// #endregion

	// #region Default Sub-components
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-button');
	#element = null;
	#overlayElement = null;
	// #endregion
}
