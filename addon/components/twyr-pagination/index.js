import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class TwyrPaginationComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	@tracked designSystem = null;
	@tracked activeClass = null;
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	// #endregion

	// #region DOM Event Handlers
	@action
	onClassListChange() {
		if(!this.#element)
			return;

		const paginationClasses = Array?.from?.(this.#element?.classList);
		let appliedClass = null;

		while(paginationClasses?.length) {
			appliedClass = paginationClasses?.pop?.();
			if(['primary', 'secondary', 'tertiary']?.includes?.(appliedClass))
				break;

			if(['success', 'info', 'warning', 'error']?.includes?.(appliedClass))
				break;

			if(['black', 'darkest', 'darker', 'dark']?.includes?.(appliedClass))
				break;

			if(['white', 'lightest', 'lighter', 'light']?.includes?.(appliedClass))
				break;

			appliedClass = null;
		}

		this.activeClass = appliedClass;
	}

	@action
	onDesignSystemChange(designSystem) {
		this.#debug(`onDesignSystemChange::designSystem: ${designSystem}`);
		this.designSystem = designSystem;

		this?.onClassListChange?.();
	}

	@action
	storeElement(element) {
		this.#debug(`storeElement: `, element);
		this.#element = element;

		this?.onClassListChange?.();
	}

	@action
	onFirst() {
		this.#debug(`onFirst`);
		this?._triggerPageChange?.(1);
	}

	@action
	onLast() {
		this.#debug(`onLast`);
		this?._triggerPageChange?.(this?.numPages);
	}

	@action
	onPrevious() {
		this.#debug(`onPrevious`);

		let nextPage = this?.currentPage - 1;
		if(nextPage <= 0) nextPage = 1;

		this?._triggerPageChange?.(nextPage);
	}

	@action
	onNext() {
		this.#debug(`onNext`);

		let nextPage = this?.currentPage + 1;
		if(nextPage > this?.numPages) nextPage = this?.numPages;

		this?._triggerPageChange?.(nextPage);
	}

	@action
	onChangePage(pageNum) {
		this.#debug(`onChangePage`);

		let nextPage = pageNum;
		if(nextPage <= 0) nextPage = 1;
		if(nextPage > this?.numPages) nextPage = this?.numPages;

		this?._triggerPageChange(nextPage);
	}
	// #endregion

	// #region Computed Properties
	get currentPage() {
		this.#debug(`currentPage: ${this?.args?.currentPage ?? 1}`);
		return this?.args?.currentPage ?? 1;
	}

	get currentPageItemStart() {
		let startItemNum = ((this?.currentPage - 1) * this?.itemsPerPage) + 1;
		if(startItemNum < 1) startItemNum = 1;

		this.#debug(`currentPageItemStart: ${startItemNum}`);
		return startItemNum;
	}

	get currentPageItemEnd() {
		let endItemNum = this?.currentPage * this?.itemsPerPage;
		if(endItemNum > this?.numItems) endItemNum = this?.numItems;

		this.#debug(`currentPageItemEnd: ${endItemNum}`);
		return endItemNum;
	}

	get itemsPerPage() {
		this.#debug(`itemsPerPage: ${this?.args?.itemsPerPage ?? 10}`);
		return this?.args?.itemsPerPage ?? 10;
	}

	get numItems() {
		this.#debug(`numItems: ${this?.args?.numItems ?? 0}`);
		return this?.args?.numItems ?? 0;
	}

	get numPages() {
		const numItems = this?.numItems ?? 0;
		const itemsPerPage = this?.itemsPerPage ?? 10;

		const totalPages = Math.ceil(numItems / itemsPerPage);

		this.#debug(`numPages: ${totalPages}`);
		return totalPages;
	}

	get onFirstPage() {
		this.#debug(`onFirstPage: ${this?.currentPage === 1}`);
		return (this?.currentPage === 1);
	}

	get onLastPage() {
		this.#debug(`onLastPage: ${this?.currentPage === this?.numPages}`);
		return (this?.currentPage === this?.numPages);
	}

	get pageNumbers() {
		const pageNumbers = [];
		const lastPageNum = this?.numPages;

		for(let idx = 0; idx < lastPageNum; idx++)
			pageNumbers[idx] = idx + 1;

		this.#debug(`numPages: `, pageNumbers);
		return pageNumbers;
	}
	// #endregion

	// #region Private Methods
	_triggerPageChange(pageNum) {
		this.#debug(`_triggerPageChange::pageNum: ${pageNum}`);
		this?.args?.onPageChange?.(pageNum);
	}
	// #endregion

	// #region Default Sub-components
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-pagination');
	#element = null;
	// #endregion
}
