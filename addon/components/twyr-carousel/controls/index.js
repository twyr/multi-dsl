import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

export default class TwyrCarouselControlsComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	// #endregion

	// #region DOM Event Handlers
	// #endregion

	// #region Computed Properties
	get prevIcon() {
		return this?.args?.leftControlIcon ?? 'angle-left';
	}

	get nextIcon() {
		return this?.args?.rightControlIcon ?? 'angle-right';
	}
	// #endregion

	// #region Private Methods
	// #endregion

	// #region Default Sub-components
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-carousel-controls');
	// #endregion
}
