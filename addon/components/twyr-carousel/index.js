import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { Promise } from 'rsvp';

import { action } from '@ember/object';
import { run } from '@ember/runloop';
import { tracked } from 'tracked-built-ins';

export default class TwyrCarouselComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	@tracked inited = false;

	items = tracked([]);
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	willDestroy() {
		this.#debug(`constructor`);

		if(this.#initRunHandle) {
			run?.cancel?.(this.#initRunHandle);
			this.#initRunHandle = null;
		}

		if(this.#nextRunHandle) {
			run?.cancel?.(this.#nextRunHandle);
			this.#nextRunHandle = null;
		}

		this.#items.length = 0;
		this.items.length = 0;

		super.willDestroy(...arguments);
	}
	// #endregion

	// #region DOM Event Handlers
	@action
	registerItem(item) {
		this.#debug(`registerItem: `, item);

		if(this.#initRunHandle) {
			run?.cancel?.(this.#initRunHandle);
			this.#initRunHandle = null;
		}

		this.#items?.push?.(item);
		this.#initRunHandle = run?.later?.(this, this?._initCarousel, {}, 2000);
	}

	@action
	unregisterItem(item) {
		this.#debug(`unregisterItem: `, item);

		if(this.#nextRunHandle) {
			run?.cancel?.(this.#nextRunHandle);
			this.#nextRunHandle = null;
		}

		this.#items?.splice?.(this.#items?.indexOf?.(item), 1);
		this.items?.splice?.(this.items?.indexOf?.(item), 1);

		this?._changeItem?.();
	}

	@action
	storeElement(element) {
		this.#debug(`storeElement: `, element);
		this.#element = element;

		if(this.#initRunHandle) {
			run?.cancel?.(this.#initRunHandle);
			this.#initRunHandle = null;
		}

		this.#initRunHandle = run?.later?.(this, this._initCarousel, {}, 2000);
	}
	// #endregion

	// #region Computed Properties
	get direction() {
		const direction = this?.args?.direction ?? 'left';
		if(['left', 'right'].includes(direction)) return direction;

		return 'left';
	}

	get indicatorItems() {
		return this.#items;
	}

	get slideShow() {
		return !!(this?.args?.slideShow ?? this.#slideshow);
	}

	get itemComponent() {
		return this?._getComputedSubcomponent?.('item');
	}
	// #endregion

	// #region Private Methods
	@action
	async _changeItem() {
		this.#debug(`_changeItem`);
		if(this?.items?.length <= 1) {
			this.#debug(`_changeItem: only one item. aborting...`);
			return;
		}

		const currentSlide = (this?.direction === 'left') ? this?.items?.[0] : this?.items?.[(this?.items?.length - 1)];
		const nextSlide = (this?.direction === 'left') ? this?.items?.[1] : this?.items?.[(this?.items?.length - 2)];

		await this?._slide(currentSlide, nextSlide, 'changing');

		if(this?.direction === 'left') {
			this.#element?.querySelector?.('div.twyr-carousel-container')?.append?.(currentSlide);
			this?.items?.push?.(this?.items?.shift?.());
		}

		if(this?.direction === 'right') {
			this.#element?.querySelector?.('div.twyr-carousel-container')?.insertBefore?.(currentSlide, this?.items[0]);
			this?.items?.unshift?.(this?.items?.pop?.());
		}
	}

	@action
	_initCarousel() {
		this.#debug(`_initCarousel`);

		Array.prototype.push.apply(this?.items, this.#items);
		if(!this?.items?.length) {
			this.#debug(`_initCarousel::no registered items. aborting...`);
			return;
		}

		const item = (this?.direction === 'left') ? this?.items?.[0] : this?.items?.[(this?.items?.length - 1)];
		item?.classList?.add('current');

		const itemStyle = window?.getComputedStyle?.(item);

		this.#element?.style?.setProperty?.('--max-width', itemStyle?.width);
		this.#element?.style?.setProperty?.('--max-height', itemStyle?.height);

		this.#debug(`_initCarousel::initing the container...`);
		this.#element?.classList?.add?.('inited');

		this.#debug(`_initCarousel::starting the carouseling...`);
		const displayInterval = Number(itemStyle?.getPropertyValue?.('--display-interval')?.trim?.()?.replace?.('ms', '') ?? '2500');
		this.#nextRunHandle = run?.later?.(this, this?._changeItem, {}, displayInterval);

		this.inited = true;
	}

	@action
	async _navigateTo(item) {
		this.#debug(`_navigateTo: `, item);

		if(this?.items?.length <= 1) {
			this.#debug(`_navigateTo: only one item. aborting...`);
			return;
		}

		const itemPosition = this?.items?.indexOf?.(item);
		if(itemPosition < 0) {
			this.#debug(`_navigateTo: cannot find item. aborting...`);
			return;
		}

		const elementStyle = window?.getComputedStyle?.(this.#element);
		const transitionInterval = elementStyle?.getPropertyValue?.('--transition-interval')?.trim?.();

		this.#element?.style?.setProperty?.('--transition-interval', `${Number(transitionInterval?.replace?.('ms', '')) * 0.3}ms`);
		while(!item?.classList?.contains?.('current'))
			await this?._changeItem();

		this.#element?.style?.setProperty?.('--transition-interval', transitionInterval);
	}

	@action
	async _navigateNext() {
		this.#debug(`_navigateNext`);
		await this?._changeItem?.();
	}

	@action
	async _navigatePrev() {
		this.#debug(`_navigatePrev`);
		if(this?.items?.length <= 1) {
			this.#debug(`_navigatePrev: only one item. aborting...`);
			return;
		}

		let currentSlide = null;
		let nextSlide = null;

		if(this?.direction === 'left') {
			this?.items?.unshift?.(this?.items?.pop?.());
			this.#element?.querySelector?.('div.twyr-carousel-container')?.insertBefore?.(this?.items?.[0], this?.items?.[1]);

			currentSlide = this?.items?.[1];
			nextSlide = this?.items?.[0];
		}

		if(this?.direction === 'right') {
			this?.items?.push?.(this?.items?.shift?.());
			this.#element?.querySelector?.('div.twyr-carousel-container')?.append?.(this?.items[(this?.items?.length - 1)]);

			currentSlide = this?.items?.[(this?.items?.length - 2)];
			nextSlide = this?.items?.[(this?.items?.length - 1)];
		}

		await this?._slide?.(currentSlide, nextSlide, 'unchanging');
	}

	@action
	_slide(currentSlide, nextSlide, transitionClass) {
		this.#debug(`_slide::current: `, currentSlide, ', next: ', nextSlide);

		return new Promise((resolve) => {
			if(this.#nextRunHandle) {
				this.#debug(`_slide: canceling scheduled changeover...`);
				run?.cancel?.(this.#nextRunHandle);
				this.#nextRunHandle = null;
			}

			if(currentSlide === nextSlide) {
				this.#debug(`_slide: only one item. aborting...`);
				return;
			}

			const elementStyle = window?.getComputedStyle?.(this.#element);
			const transitionInterval = Number(elementStyle?.getPropertyValue?.('--transition-interval')?.trim?.()?.replace?.('ms', '') ?? '600');

			currentSlide?.classList?.add?.('prev');
			currentSlide?.classList?.remove?.('current');
			nextSlide?.classList?.add?.('next');

			const itemStyle = window?.getComputedStyle?.(nextSlide);
			this.#element?.style?.setProperty?.('--max-width', itemStyle?.width);
			this.#element?.style?.setProperty?.('--max-height', itemStyle?.height);

			currentSlide?.classList?.add?.(transitionClass);
			nextSlide?.classList?.add?.(transitionClass);

			this.#nextRunHandle = run?.later?.(this, () => {
				this.#nextRunHandle = null;

				nextSlide?.classList?.add?.('current');
				nextSlide?.classList?.remove?.('next', transitionClass);

				currentSlide?.classList?.remove?.('prev', transitionClass);

				const newElementStyle = window?.getComputedStyle?.(this.#element);
				const containerStyle = window?.getComputedStyle?.(this.#element?.parentNode);

				const newElementWidth = Number(newElementStyle?.width?.trim?.()?.replace?.('px', ''));
				const newElementHeight = Number(newElementStyle?.height?.trim?.()?.replace?.('px', ''));

				const containerWidth = Number(containerStyle?.width?.trim?.()?.replace?.('px', ''));
				const containerHeight = Number(containerStyle?.height?.trim?.()?.replace?.('px', ''));

				if(newElementWidth > containerWidth) this.#element?.style?.setProperty?.('--max-width', containerStyle?.width);
				if(newElementHeight > containerHeight) this.#element?.style?.setProperty?.('--max-height', containerStyle?.height);

				if(!this?.slideShow) return;

				let displayInterval = Number(itemStyle?.getPropertyValue?.('--display-interval')?.trim?.()?.replace?.('ms', '') ?? '0');
				if(!displayInterval) displayInterval = 2500;

				this.#nextRunHandle = run?.later?.(this, this?._changeItem, {}, displayInterval);
				resolve();
			}, {}, transitionInterval);
		});
	}

	_getComputedSubcomponent(componentName) {
		const subComponent = this?.args?.customComponents?.[componentName] ?? this.#subComponents?.[componentName];
		this.#debug(`${componentName}-component`, subComponent);

		return subComponent;
	}
	// #endregion

	// #region Default Sub-components
	#subComponents = {
		'item': 'twyr-carousel/item'
	};
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-carousel');
	#element = null;

	#initRunHandle = null;
	#items = [];

	#slideshow = true;
	#nextRunHandle = null;
	// #endregion
}
