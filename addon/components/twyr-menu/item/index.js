import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { run } from '@ember/runloop';

export default class TwyrMenuItemComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	dropdownControls = null;
	dropdownStatus = null;
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	// #endregion

	// #region DOM Event Handlers
	@action
	closeMenu(event) {
		this.#debug(`closeMenu: `, event);
		this?.args?.menu?.close?.(event);
	}

	@action
	setDropdownControls(controls) {
		this.#debug(`setDropdownControls::controls: `, controls);
		this.dropdownControls = controls;

		if(this?.args?.parentMenuItem) {
			this.#debug(`setDropdownControls::not a top-level item. parent is: `, this?.args?.parentMenuItem);
			return;
		}

		if(!this.#element) {
			this.#debug(`setDropdownControls::element not yet set. deferring registration...`);
			return;
		}

		this.#debug(`setDropdownControls::is a top-level item::registering with menu`);
		this?.args?.registerWithMenu?.(this.#element, this.dropdownControls);
	}

	@action
	setDropdownStatus(status) {
		this.#debug(`setDropdownStatus: `, status);
		this.dropdownStatus = status;

		if(this?.args?.parentMenuItem) {
			this.#debug(`setDropdownStatus::not a top-level item. parent is: `, this?.args?.parentMenuItem);
			return;
		}

		this.#debug(`setDropdownStatus::is a top-level item::setting status: `, this.dropdownStatus);
		this?.args?.menu?.setStatus?.(this.#element, this.dropdownStatus);
	}

	@action
	storeElement(element) {
		this.#debug(`storeElement: `, element);
		this.#element = element;

		if(!this.dropdownControls) {
			this.#debug(`storeElement::controls not yet set. deferring registration...`);
			return;
		}

		this.#debug(`storeElement::is a top-level item::registering with menu`);
		this?.args?.registerWithMenu?.(this.#element, this.dropdownControls);
	}

	@action
	handleMouseEnter(event) {
		this.#debug(`handleMouseEnter: `, event);

		document.removeEventListener('mousemove', this?.handleMouseMove, true);
		if(!this?.args?.parentMenuItem && (this?.args?.triggerEvent === 'click'))
			return;

		const isEventInsideElement = ((event?.target === this.#element) || this.#element?.contains?.(event?.target));
		if(!isEventInsideElement) return;

		if(this?.dropdownStatus?.open) return;

		document.addEventListener('mousemove', this?.handleMouseMove, true);
		this?.dropdownControls?.open?.(event);
	}

	@action
	handleMouseLeave(event) {
		this.#debug(`handleMouseLeave: `, event);
		if(!this?.args?.parentMenuItem) {
			this.#debug(`handleMouseLeave: top-level menu-item. aborting...`);
			document.removeEventListener('mousemove', this?.handleMouseMove, true);

			return;
		}

		const isEventInsideElement = ((event?.target === this.#element) || this.#element?.contains?.(event?.target));
		if(!isEventInsideElement) {
			this.#debug(`handleMouseLeave: mouse leave is outside this element. aborting...`);
			return;
		}

		run.later(this, () => {
			const isMouseInsideElement = this.#element?.contains?.(this.#currentMousePosition?.target);
			if(isMouseInsideElement) {
				this.#debug(`handleMouseLeave: mouse is still inside this menu-item. aborting...`);
				return;
			}

			if(!this?.dropdownStatus?.open) {
				this.#debug(`handleMouseLeave: dropdown is already closed. aborting...`);
				return;
			}

			this.#debug(`handleMouseLeave: closing the dropdown...`);
			document.removeEventListener('mousemove', this?.handleMouseMove, true);
			this?.dropdownControls?.close?.(event);
		}, {}, 50);
	}

	@action
	handleMouseMove(event) {
		// this.#debug(`handleMouseMove: `, event);
		this.#currentMousePosition = event;
	}
	// #endregion

	// #region Computed Properties
	get itemListComponent() {
		return this?._getComputedSubcomponent?.('itemList');
	}

	get itemTriggerComponent() {
		return this?._getComputedSubcomponent?.('itemTrigger');
	}
	// #endregion

	// #region Private Methods
	_getComputedSubcomponent(componentName) {
		const subComponent = this?.args?.customComponents?.[componentName] ?? this.#subComponents?.[componentName];
		this.#debug(`${componentName}-component`, subComponent);

		return subComponent;
	}
	// #endregion

	// #region Default Sub-components
	#subComponents = {
		'itemList': 'twyr-menu/item-list',
		'itemTrigger': 'twyr-menu/item-trigger'
	};
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-menu-item');
	#element = null;

	#currentMousePosition = null;
	// #endregion
}
