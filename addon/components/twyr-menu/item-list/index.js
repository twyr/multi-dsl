import Component from './../../twyr-basic-dropdown/content';
import debugLogger from 'ember-debug-logger';

export default class TwyrMenuItemListComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	// #endregion

	// #region DOM Event Handlers
	// #endregion

	// #region Computed Properties
	get itemComponent() {
		return this?._getComputedSubcomponent?.('item');
	}
	// #endregion

	// #region Private Methods
	_getComputedSubcomponent(componentName) {
		const subComponent = this?.args?.customComponents?.[componentName] ?? this.#subComponents?.[componentName];
		this.#debug(`${componentName}-component`, subComponent);

		return subComponent;
	}
	// #endregion

	// #region Default Sub-components
	#subComponents = {
		'item': 'twyr-menu/item'
	};
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-menu-item-list');
	// #endregion
}
