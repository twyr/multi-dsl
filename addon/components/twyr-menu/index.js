import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class TwyrMenuComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	controls = {};

	@tracked open = false;
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);

		this.controls.close = this?.closeSubmenus;
		this.controls.setStatus = this?.setStatus;
	}
	// #endregion

	// #region Lifecycle Hooks
	willDestroy() {
		this.#debug(`willDestroy`);

		this.#menuItems?.clear?.();
		super.willDestroy(...arguments);
	}
	// #endregion

	// #region DOM Event Handlers
	@action
	closeSubmenus(event) {
		this.#debug(`closeSubmenus::args: `, arguments);
		if(!this?.open) return;

		this.#menuItems?.forEach?.((menuItemControls) => {
			menuItemControls?.close?.(event);
		});
	}

	@action
	setStatus(menuItemElement, status) {
		this.#debug(`setStatus::args: `, status);

		let isMenuOpen = false;
		this.#menuItems?.forEach?.((otherMenuItemControls, otherMenuItemElement) => {
			if(otherMenuItemElement !== menuItemElement)
				otherMenuItemControls?.close?.();

			isMenuOpen = isMenuOpen || otherMenuItemControls?.isOpen?.();
		});

		this.#debug(`setStatus::isOpen: `, isMenuOpen);
		this.open = isMenuOpen;
	}

	@action
	registerMenuItem(menuItemElement, menuItemControls) {
		this.#menuItems?.set(menuItemElement, menuItemControls);
	}
	// #endregion

	// #region Computed Properties
	get menuControls() {
		return this.controls;
	}

	get itemComponent() {
		return this?._getComputedSubcomponent?.('item');
	}

	get triggerEvent() {
		const triggerEvent = this?.open ? 'hover' : 'click';
		this.#debug(`triggerEvent: `, triggerEvent);

		return triggerEvent;
	}
	// #endregion

	// #region Private Methods
	_getComputedSubcomponent(componentName) {
		const subComponent = this?.args?.customComponents?.[componentName] ?? this.#subComponents?.[componentName];
		this.#debug(`${componentName}-component`, subComponent);

		return subComponent;
	}
	// #endregion

	// #region Default Sub-components
	#subComponents = {
		'item': 'twyr-menu/item'
	};
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-menu');
	#menuItems = new Map();
	// #endregion
}
