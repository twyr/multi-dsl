import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { run } from '@ember/runloop';

export default class TwyrBadgeComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	willDestroy() {
		this.#debug(`willDestroy`);

		this.#element.parentNode.style.overflow = this.#parentOverflowStyle;
		super.willDestroy(...arguments);
	}
	// #endregion

	// #region DOM Event Handlers
	@action
	storeElement(element) {
		this.#debug(`storeElement: `, element);
		this.#element = element;

		run?.later?.(this, this?.positionCalculator, {}, 100);
	}
	// #endregion

	// #region Computed Properties
	get positionCalculator() {
		return this?.args?.badgePositionCalculator ?? this._calcBadgePosition;
	}
	// #endregion

	// #region Private Methods
	@action
	_calcBadgePosition() {
		const classes = this.#element?.className?.split?.(' ')?.filter?.((className) => { return !!className; })?.map?.((className) => { return className?.trim?.(); });
		let currentClass = classes.pop();

		do {
			if(currentClass === 'floating')
				break;

			currentClass = classes.pop();
		} while(classes.length);

		if(currentClass !== 'floating')
			return;

		const elementStyle = `
			top: 0px;
			left: 100%;
			transform: translate(-50%,-50%);
		`;

		this.#element.style.cssText = elementStyle;

		this.#parentOverflowStyle = this.#element?.parentNode?.style?.overflow;
		this.#element.parentNode.style.overflow = 'visible';
	}
	// #endregion

	// #region Default Sub-components
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-badge');
	#element = null;

	#parentOverflowStyle = null;
	// #endregion
}
