import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { run } from '@ember/runloop';
import { tracked } from '@glimmer/tracking';

export default class TwyrBreadcrumbContainerCrumbComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	@tracked displayElement = null;
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);

		this.#controls.setDisplayElement = this._setDisplayElement;
	}
	// #endregion

	// #region Lifecycle Hooks
	willDestroy() {
		this.#debug(`willDestroy`);

		this?.breadcrumbContainer?.remove?.(this.#element);
		super.willDestroy(...arguments);
	}
	// #endregion

	// #region DOM Event Handlers
	@action
	storeElement(element) {
		this.#debug(`storeElement: `, element);
		this.#element = element;

		run?.later?.(this, () => {
			const breadcrumbContainer = this?.breadcrumbContainer;
			breadcrumbContainer?.add?.(this.#element, this.#controls);
		}, {}, 500);
	}
	// #endregion

	// #region Computed Properties
	get breadcrumbContainer() {
		this.#debug(`breadcrumbContainer`);

		if(!this.#element) {
			this.#debug(`breadcrumbContainer: element uninitialized. returning first available in the document`);
			return document?.querySelector?.('ul.twyr-breadcrumb-container')?.breadcrumbControls;
		}

		let currentParent = this.#element;
		let breadcrumbContainer = null;

		do {
			breadcrumbContainer = currentParent?.querySelector?.('ul.twyr-breadcrumb-container');
			if(breadcrumbContainer) break;

			currentParent = currentParent.parentNode;
		} while(currentParent.tagName.toLowerCase() !== 'body');

		if(breadcrumbContainer) {
			this.#debug(`breadcrumbContainerElement: `, breadcrumbContainer);
			return breadcrumbContainer?.breadcrumbControls;
		}

		breadcrumbContainer = document?.querySelector?.('ul.twyr-breadcrumb-container');
		this.#debug(`breadcrumbContainerElement: none found in ancestors. first available in the document is: `, breadcrumbContainer);

		return breadcrumbContainer?.breadcrumbControls;
	}
	// #endregion

	// #region Private Methods
	@action
	_setDisplayElement(dispElem) {
		this.#debug(`setDisplayElement: `, dispElem);
		this.displayElement = dispElem;
	}
	// #endregion

	// #region Default Sub-components
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-breadcrumb-container-crumb');
	#element = null;
	#controls = {};
	// #endregion
}
