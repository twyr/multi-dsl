import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { htmlSafe } from '@ember/template';
import { tracked } from 'tracked-built-ins';

export default class TwyrBreadcrumbContainerComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	crumbs = tracked(Map);
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);

		this.#controls['add'] = this?._addCrumb;
		this.#controls['remove'] = this?._removeCrumb;
	}
	// #endregion

	// #region Lifecycle Hooks
	willDestroy() {
		this.#debug(`willDestroy`);

		this.#element.breadcrumbControls = null;
		this?.crumbs?.clear?.();

		super.willDestroy(...arguments);
	}
	// #endregion

	// #region DOM Event Handlers
	@action
	onDesignSystemChange(designSystem) {
		this.#debug(`onDesignSystemChange::designSystem: ${designSystem}`);
		this.#designSystem = designSystem;

		this.#element?.style?.setProperty?.('--divider-symbol', this?.divider);
	}

	@action
	setCrumbElement(crumbElement, crumbEntry) {
		this.#debug(`setCrumbElement: `, crumbElement, crumbEntry);

		const crumbControls = crumbEntry?.[1];
		crumbControls?.setDisplayElement?.(crumbElement);
	}

	@action
	storeElement(element) {
		this.#debug(`storeElement: `, element);

		this.#element = element;
		this.#element.breadcrumbControls = this.#controls;

		this.#element?.style?.setProperty?.('--divider-symbol', this?.divider);
	}
	// #endregion

	// #region Computed Properties
	get divider() {
		let divider = this?.args?.divider;
		if(!divider)
		switch (this.#designSystem) {
			case 'bootstrap':
				divider = '/';
				break;

			default:
				divider = '>';
				break;
		}

		// eslint-disable-next-line no-nested-ternary
		return divider?.startsWith?.('url') ? divider : (divider?.startsWith?.(`"`) ? htmlSafe(divider) : htmlSafe(`"${divider}"`));
	}
	// #endregion

	// #region Private Methods
	@action
	_addCrumb(crumbElement, crumbControls) {
		this.#debug(`_addCrumb: `, crumbElement);
		this?.crumbs?.set?.(crumbElement, crumbControls);
	}

	@action
	_removeCrumb(crumbElement) {
		this.#debug(`_removeCrumb: `, crumbElement);
		this?.crumbs?.delete?.(crumbElement);
	}
	// #endregion

	// #region Default Sub-components
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-breadcrumb-container');
	#element = null;

	#designSystem = null;
	#controls = {};
	// #endregion
}
