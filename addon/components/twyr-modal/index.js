import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';

export default class TwyrModalComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	// #endregion

	// #region DOM Event Handlers
	@action
	onClickOutside(event) {
		const shouldClose = this?.args?.clickOutsideToClose ?? true;
		if(!shouldClose) {
			this.#debug(`onClickOutside: ${shouldClose}. aborting...`);
			return;
		}

		const isEventInsideElement = ((event?.target === this.#element) || this.#element?.contains?.(event?.target));
		if(isEventInsideElement) {
			this.#debug(`onClickOutside: click is inside this element. aborting...`);
			return;
		}

		this?.args?.onClose?.(event);
	}

	@action
	storeElement(element) {
		this.#debug(`storeElement: `, element);
		this.#element = element;

		this?.positionModal?.();
	}

	@action
	positionModal() {
		let positionClass = 'absolute';
		let heightClass = 'max-h-full';

		if(this?.parentElement === document.body) {
			positionClass = 'fixed';
			heightClass = 'max-h-screen';
		}

		this.#element?.parentNode?.classList?.add?.(positionClass, this?.xAlign, this?.yAlign);
		this.#element?.classList?.add(heightClass);
	}
	// #endregion

	// #region Computed Properties
	get backgroundFullScreen() {
		return (this?.parentElement === document.body);
	}

	get xAlign() {
		return this?.args?.xAlign ?? 'center';
	}

	get yAlign() {
		return this?.args?.yAlign ?? 'middle';
	}

	get parentElement() {
		return this?.args?.parentElement ?? document.body;
	}

	get headerComponent() {
		return this?._getComputedSubcomponent?.('header');
	}

	get bodyComponent() {
		return this?._getComputedSubcomponent?.('body');
	}

	get footerComponent() {
		return this?._getComputedSubcomponent?.('footer');
	}
	// #endregion

	// #region Private Methods
	_getComputedSubcomponent(componentName) {
		const subComponent = this?.args?.customComponents?.[componentName] ?? this.#subComponents?.[componentName];
		this.#debug(`${componentName}-component`, subComponent);

		return subComponent;
	}
	// #endregion

	// #region Default Sub-components
	#subComponents = {
		'header': 'twyr-modal/header',
		'body': 'twyr-modal/body',
		'footer': 'twyr-modal/footer'
	};
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-modal');
	#element = null;
	// #endregion
}
