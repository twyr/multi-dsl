import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { run } from '@ember/runloop';
import { tracked } from '@glimmer/tracking';

export default class TwyrAccordionItemComponent extends Component {
	// #region Accessed Services
	@service('colourUtilities') colourUtilities;
	// #endregion

	// #region Tracked Attributes
	itemControls = {};
	@tracked designSystem = null;
	@tracked expanded = false;
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);

		this.itemControls.close = this.close;
		this.expanded = this?.args?.expanded ?? false;
	}
	// #endregion

	// #region Lifecycle Hooks
	// #endregion

	// #region DOM Event Handlers
	@action
	close() {
		this.#debug(`close`);
		this.expanded = false;

		const triggerElement = this.#element?.querySelector?.('.twyr-accordion-item-trigger');
		triggerElement.style.backgroundColor = this.#originalTriggerBackground;

		this.#originalTriggerBackground = null;
	}

	@action
	onDesignSystemChange(newDesignSystem) {
		this.#debug(`onDesignSystemChange::newDesignSystem: ${newDesignSystem}`);

		const triggerElement = this.#element?.querySelector?.('.twyr-accordion-item-trigger');
		triggerElement.style.backgroundColor = this.#originalTriggerBackground;
		this.#originalTriggerBackground = null;

		this.designSystem = newDesignSystem;
		if(!this?.expanded) return;

		if((this?.designSystem !== 'bootstrap') && (this?.designSystem !== 'tailwind'))
			return;

		const buttonElement = triggerElement?.querySelector?.('.twyr-button');
		const overlayColour = this?.colourUtilities?.getOverlayColour?.(buttonElement);

		this.#originalTriggerBackground = window?.getComputedStyle?.(triggerElement)?.style?.backgroundColor;
		triggerElement.style.backgroundColor = overlayColour;
	}

	@action
	toggleState() {
		this.#debug(`toggleState`, !this.expanded);

		const elementStyle = window.getComputedStyle(this.#element);

		let transitionDuration = Number(elementStyle?.getPropertyValue?.('--transition-interval')?.replace('ms', '')?.trim?.() ?? '0');
		if(!transitionDuration) transitionDuration = 600;

		this?.args?.accordion?.setStatus?.(this.#element, !this?.expanded);
		// eslint-disable-next-line curly
		if(this?.args?.multipleOpen) {
			this.expanded = !this?.expanded;
		}
		// eslint-disable-next-line curly
		else {
			// eslint-disable-next-line no-lonely-if
			if(this?.expanded)
				this.expanded = !this?.expanded;
			// eslint-disable-next-line curly
			else {
				run?.later?.(this, () => {
					this.expanded = !this.expanded;
				}, {}, transitionDuration);
			}
		}

		if((this?.designSystem !== 'bootstrap') && (this?.designSystem !== 'tailwind'))
			return;

		if(!this?.expanded) {
			const triggerElement = this.#element?.querySelector?.('.twyr-accordion-item-trigger');
			triggerElement.style.backgroundColor = this.#originalTriggerBackground;

			this.#originalTriggerBackground = null;
			return;
		}

		const triggerElement = this.#element?.querySelector?.('.twyr-accordion-item-trigger');
		const buttonElement = triggerElement?.querySelector?.('.twyr-button');

		this.#originalTriggerBackground = window?.getComputedStyle?.(triggerElement)?.style?.backgroundColor;

		const overlayColour = this?.colourUtilities?.getOverlayColour?.(buttonElement);
		triggerElement.style.backgroundColor = overlayColour;
	}

	@action
	storeElement(element) {
		this.#debug(`storeElement: `, element);
		this.#element = element;

		this?.args?.registerWithAccordion?.(this.#element, this.itemControls);
		this?.args?.accordion?.setStatus?.(this.#element, this.expanded);

		if((this?.designSystem !== 'bootstrap') && (this?.designSystem !== 'tailwind'))
			return;

		if(!this?.expanded)
			return;

		const triggerElement = this.#element?.querySelector?.('.twyr-accordion-item-trigger');
		this.#originalTriggerBackground = window?.getComputedStyle?.(triggerElement)?.style?.backgroundColor;

		const buttonElement = triggerElement?.querySelector?.('.twyr-button');
		const overlayColour = this?.colourUtilities?.getOverlayColour?.(buttonElement);

		triggerElement.style.backgroundColor = overlayColour;
	}
	// #endregion

	// #region Computed Properties
	get titleComponent() {
		return this?._getComputedSubcomponent?.('title');
	}
	// #endregion

	// #region Private Methods
	_getComputedSubcomponent(componentName) {
		const subComponent = this?.args?.customComponents?.[componentName] ?? this.#subComponents?.[componentName];
		this.#debug(`${componentName}-component`, subComponent);

		return subComponent;
	}
	// #endregion

	// #region Default Sub-components
	#subComponents = {
		'title': 'twyr-sub-headline'
	};
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-accordion-item');
	#element = null;

	#originalTriggerBackground = null;
	// #endregion
}
