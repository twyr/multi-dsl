import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';

export default class TwyrAccordionComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);

		this.#controls.setStatus = this?.setAccordionItemStatus;
	}
	// #endregion

	// #region Lifecycle Hooks
	willDestroy() {
		this.#debug(`willDestroy`);

		this.#accordionItems?.clear?.();
		super.willDestroy(...arguments);
	}
	// #endregion

	// #region DOM Event Handlers
	@action
	setAccordionItemStatus(accordionItemElement, expanded) {
		this.#debug(`setStatus::args: `, status);

		if(!expanded) return;
		if(this.isMultipleOpen) return;

		this.#accordionItems?.forEach?.((otherAccordionItemControls, otherAccordionItemElement) => {
			if(accordionItemElement === otherAccordionItemElement)
				return;

			otherAccordionItemControls?.close?.();
		});
	}

	@action
	registerAccordionItem(accordionItemElement, accordionItemControls) {
		this.#accordionItems?.set(accordionItemElement, accordionItemControls);
	}
	// #endregion

	// #region Computed Properties
	get accordionControls() {
		return this.#controls;
	}

	get isMultipleOpen() {
		return this?.args?.multiple ?? false;
	}

	get itemComponent() {
		return this?._getComputedSubcomponent?.('item');
	}
	// #endregion

	// #region Private Methods
	_getComputedSubcomponent(componentName) {
		const subComponent = this?.args?.customComponents?.[componentName] ?? this.#subComponents?.[componentName];
		this.#debug(`${componentName}-component`, subComponent);

		return subComponent;
	}
	// #endregion

	// #region Default Sub-components
	#subComponents = {
		'item': 'twyr-accordion/item'
	};
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-accordion');

	#accordionItems = new Map();
	#controls = {};
	// #endregion
}
