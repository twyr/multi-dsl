import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class TwyrListToolbarComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	@tracked designSystem = null;
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	willDestroy() {
		this.#debug(`willDestroy`);
		super.willDestroy(...arguments);
	}
	// #endregion

	// #region DOM Event Handlers
	@action
	onDesignSystemChange(newDesignSystem) {
		this.#debug(`onDesignSystemChange::newDesignSystem: ${newDesignSystem}`);
		this.designSystem = newDesignSystem;
	}

	@action
	toggleAll() {
		this.#debug(`toggleAll`);
		if(this?.args?.expanded)
			this?.args?.listControls?.closeAll?.();
		else
			this?.args?.listControls?.openAll?.();
	}
	// #endregion

	// #region Computed Properties
	get iconComponent() {
		return this?._getComputedSubcomponent?.('icon');
	}

	get menuComponent() {
		return this?._getComputedSubcomponent?.('menu');
	}

	get thumbnailComponent() {
		return this?._getComputedSubcomponent?.('thumbnail');
	}
	// #endregion

	// #region Private Methods
	_getComputedSubcomponent(componentName) {
		const subComponent = this?.args?.customComponents?.[componentName] ?? this.#subComponents?.[componentName];
		this.#debug(`${componentName}-component`, subComponent);

		return subComponent;
	}
	// //#endregion

	// #region Default Sub-components
	#subComponents = {
		'thumbnail': 'twyr-list/thumbnail',
		'icon': 'twyr-icon',
		'menu': 'twyr-menu'
	};
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-list-toolbar');
	// #endregion
}
