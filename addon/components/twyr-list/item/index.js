import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class TwyrListItemComponent extends Component {
	// #region Accessed Services
	@service('colourUtilities') colourUtilities;
	// #endregion

	// #region Tracked Attributes
	@tracked designSystem = null;
	@tracked expanded = false;
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);

		this.#controls.close = this._close;
		this.#controls.open = this._open;
		this.#controls.expanded = this?.expanded ?? false;
		this.#controls.hasContentBlock = false;
	}
	// #endregion

	// #region Lifecycle Hooks
	willDestroy() {
		this.#debug(`willDestroy`);

		this?.args?.unregisterWithList?.(this.#element);
		super.willDestroy(...arguments);
	}
	// #endregion

	// #region DOM Event Handlers
	@action
	onDesignSystemChange(newDesignSystem) {
		this.#debug(`onDesignSystemChange::newDesignSystem: ${newDesignSystem}`);
		this.designSystem = newDesignSystem;
	}

	@action
	storeElement(element) {
		this.#debug(`storeElement: `, element);
		this.#element = element;

		this?.args?.registerWithList?.(this.#element, this.#controls);
	}

	@action
	hasContentBlock(hasIt) {
		this.#debug(`hasContentBlock: `, hasIt);
		this.#controls.hasContentBlock = hasIt;

		this?.args?.registerWithList?.(this.#element, this.#controls);
	}

	@action
	toggle() {
		if(this.expanded)
			this._close();
		else
			this._open();
	}

	@action
	onMouseEnter(event) {
		this.#debug(`onMouseEnter`, event);

		if(this?.designSystem !== 'material') {
			this.#debug(`onMouseEnter: design system is not "material" - aborting...`);
			return;
		}

		this?._setupOverlay?.();
		this.#debug(`onMouseEnter::applied the overlay`);
	}

	@action
	onMouseLeave(event) {
		this.#debug(`onMouseLeave`, event);

		if(this?.designSystem !== 'material') {
			this.#debug(`onMouseEnter: design system is not "material" - aborting...`);
			return;
		}

		if(!this.#overlayElement)
			return;

		this.#element?.removeChild?.(this.#overlayElement);
		this.#overlayElement?.remove?.();
		this.#overlayElement = null;
	}
	// #endregion

	// #region Computed Properties
	get thumbnailComponent() {
		return this?._getComputedSubcomponent?.('thumbnail');
	}

	get iconComponent() {
		return this?._getComputedSubcomponent?.('icon');
	}

	get headlineComponent() {
		return this?._getComputedSubcomponent?.('headline');
	}

	get subHeadlineComponent() {
		return this?._getComputedSubcomponent?.('subheadline');
	}

	get menuComponent() {
		return this?._getComputedSubcomponent?.('menu');
	}
	// #endregion

	// #region Private Methods
	@action
	_close() {
		this.#debug(`_close`);
		if(!this?.args?.expandable)
			return;

		this.expanded = false;

		this.#controls.expanded = this?.expanded;
		this?.args?.registerWithList?.(this.#element, this.#controls);
	}

	@action
	_open() {
		this.#debug(`_open`);
		if(!this?.args?.expandable)
			return;

		this.expanded = true;

		this.#controls.expanded = this?.expanded;
		this?.args?.registerWithList?.(this.#element, this.#controls);
	}

	_setupOverlay() {
		const overlayColour = this?.colourUtilities?.getOverlayColour?.(this.#element, null, '0.05');
		this.#debug(`_setupOverlay::overlayColour: `, overlayColour);

		const overlay = this.#overlayElement ?? document?.createElement?.('div');
		overlay?.classList?.add?.('twyr-list-item-overlay');

		const width = this.#element.clientWidth;
		const height = this.#element.clientHeight;

		const overlayCss = `
			position:absolute;
			left: 0px;
			top: 0px;
			width: ${width}px;
			height: ${height}px;
			background-color: ${overlayColour};
			border-color: ${overlayColour};
			pointer-events: none;
		`;

		overlay.style.cssText = overlayCss;

		this.#overlayElement = overlay;
		this.#element?.appendChild?.(this.#overlayElement);
	}

	_getComputedSubcomponent(componentName) {
		const subComponent = this?.args?.customComponents?.[componentName] ?? this.#subComponents?.[componentName];
		this.#debug(`${componentName}-component`, subComponent);

		return subComponent;
	}
	// //#endregion

	// #region Default Sub-components
	#subComponents = {
		'thumbnail': 'twyr-list/thumbnail',
		'icon': 'twyr-icon',
		'headline': 'twyr-headline',
		'subheadline': 'twyr-sub-headline',
		'menu': 'twyr-menu'
	};
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-list-item');
	#element = null;
	#overlayElement = null;

	#controls = {};
	// #endregion
}
