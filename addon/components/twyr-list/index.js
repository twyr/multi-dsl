import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class TwyrListComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	@tracked expanded = false;
	@tracked hasContentBlocks = false;
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);

		this.#controls.closeAll = this?._closeAll;
		this.#controls.openAll = this?._openAll;
	}
	// #endregion

	// #region Lifecycle Hooks
	willDestroy() {
		this.#debug(`willDestroy`);

		this.#items?.clear?.();
		super.willDestroy(...arguments);
	}
	// #endregion

	// #region DOM Event Handlers
	@action
	registerItem(item, controls) {
		this.#debug(`registerItem: `, item, controls);
		this.#items?.set?.(item, controls);

		let allExpanded = true;
		let hasContentBlocks = false;

		this.#items?.forEach?.((value) => {
			const itemControls = value;

			hasContentBlocks = hasContentBlocks || itemControls?.hasContentBlock;
			if(itemControls?.hasContentBlock) allExpanded = allExpanded && itemControls?.expanded;
		});

		this.expanded = allExpanded;
		this.hasContentBlocks = hasContentBlocks;
	}

	@action
	unregisterItem(item) {
		this.#debug(`unregisterItem:`, item);
		this.#items?.delete?.(item);

		let allExpanded = true;

		this.#items?.forEach?.((value) => {
			const itemControls = value;
			allExpanded = allExpanded && itemControls?.expanded;
		});

		this.expanded = allExpanded;
	}
	// #endregion

	// #region Computed Properties
	get listControls() {
		return this.#controls;
	}

	get isOrdered() {
		return !!this?.args?.ordered ?? false;
	}

	get itemComponent() {
		return this?._getComputedSubcomponent?.('item');
	}

	get toolbarComponent() {
		return this?._getComputedSubcomponent?.('toolbar');
	}
	// #endregion

	// #region Private Methods
	@action
	_closeAll() {
		this.#debug(`_closeAll`);
		if(!this?.args?.expandable)
			return;

		this.#items?.forEach?.((value) => {
			const itemControls = value;
			itemControls?.close?.();
		});

		this.expanded = false;
	}

	@action
	_openAll() {
		this.#debug(`_openAll`);
		if(!this?.args?.expandable)
			return;

		this.#items?.forEach?.((value) => {
			const itemControls = value;
			itemControls?.open?.();
		});

		this.expanded = true;
	}

	_getComputedSubcomponent(componentName) {
		const subComponent = this?.args?.customComponents?.[componentName] ?? this.#subComponents?.[componentName];
		this.#debug(`${componentName}-component`, subComponent);

		return subComponent;
	}
	// #endregion

	// #region Default Sub-components
	#subComponents = {
		'item': 'twyr-list/item',
		'toolbar': 'twyr-list/toolbar'
	};
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-list');
	#items = new Map();

	#controls = {}
	// #endregion
}
