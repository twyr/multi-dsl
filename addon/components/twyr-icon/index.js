import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class TwyrIconComponent extends Component {
	// #region Accessed Services
	@service('iconClassResolver') iconClassResolver;
	// #endregion

	// #region Tracked Attributes
	@tracked designSystem = null;
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	// #endregion

	// #region DOM Event Handlers
	@action
	onDesignSystemChange(designSystem) {
		this.#debug(`onDesignSystemChange::designSystem: ${designSystem}`);
		this.designSystem = designSystem;
	}

	@action
	storeElement(element) {
		this.#debug(`storeElement: `, element);
		this.#element = element;
	}
	// #endregion

	// #region Computed Properties
	get iconClass() {
		const iconClass = this?.iconClassResolver?.getIconClass?.(this?.args?.icon, this?.designSystem);
		this.#debug(`iconClass: ${this?.args?.icon}, design system: ${this.designSystem}, icon class: ${iconClass}`);

		return iconClass;
	}

	get spinClass() {
		const animateClasses = [];
		if(this?.args?.spin) animateClasses?.push?.('animate-spin');
		if(this?.args?.reverseSpin) animateClasses?.push?.('animate-spin-reverse');
		if(this?.args?.ping) animateClasses?.push?.('animate-ping');
		if(this?.args?.pulse) animateClasses?.push?.('animate-pulse');

		return animateClasses.join(' ');
	}
	// #endregion

	// #region Private Methods
	// #endregion

	// #region Default Sub-components
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-icon');
	#element = null;
	// #endregion
}
