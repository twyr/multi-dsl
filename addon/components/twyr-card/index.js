import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

export default class TwyrCardComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	// #endregion

	// #region DOM Event Handlers
	// #endregion

	// #region Computed Properties
	get headerComponent() {
		return this?._getComputedSubcomponent?.('header');
	}

	get mediaComponent() {
		return this?._getComputedSubcomponent?.('media');
	}

	get bodyComponent() {
		return this?._getComputedSubcomponent?.('body');
	}

	get footerComponent() {
		return this?._getComputedSubcomponent?.('footer');
	}
	// #endregion

	// #region Private Methods
	_getComputedSubcomponent(componentName) {
		const subComponent = this?.args?.customComponents?.[componentName] ?? this.#subComponents?.[componentName];
		this.#debug(`${componentName}-component`, subComponent);

		return subComponent;
	}
	// #endregion

	// #region Default Sub-components
	#subComponents = {
		'header': 'twyr-card/header',
		'media': 'twyr-card/media',
		'body': 'twyr-card/body',
		'footer': 'twyr-card/footer'
	};
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-card');
	// #endregion
}
