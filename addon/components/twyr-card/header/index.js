import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

export default class TwyrCardHeaderComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	// #endregion

	// #region DOM Event Handlers
	// #endregion

	// #region Computed Properties
	get thumbnailComponent() {
		return this?._getComputedSubcomponent?.('thumbnail');
	}

	get headlineComponent() {
		return this?._getComputedSubcomponent?.('headline');
	}

	get subHeadlineComponent() {
		return this?._getComputedSubcomponent?.('subheadline');
	}

	get menuComponent() {
		return this?._getComputedSubcomponent?.('menu');
	}

	get thumbnailLeft() {
		return (this?.args?.thumbnailLeft ?? true);
	}

	get menuLeft() {
		return (this?.args?.menuLeft ?? false);
	}
	// #endregion

	// #region Private Methods
	_getComputedSubcomponent(componentName) {
		const subComponent = this?.args?.customComponents?.[componentName] ?? this.#subComponents?.[componentName];
		this.#debug(`${componentName}-component`, subComponent);

		return subComponent;
	}
	// //#endregion

	// #region Default Sub-components
	#subComponents = {
		'thumbnail': 'twyr-card/header/thumbnail',
		'headline': 'twyr-headline',
		'subheadline': 'twyr-sub-headline',
		'menu': 'twyr-menu'
	};
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-card-header');
	// #endregion
}
