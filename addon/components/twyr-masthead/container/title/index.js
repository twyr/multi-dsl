import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { inject as service } from '@ember/service';

export default class TwyrMastheadContainerTitleComponent extends Component {
	// #region Accessed Services
	@service('twyrOperatingParameters') operatingParameters;
	// #endregion

	// #region Tracked Attributes
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	// #endregion

	// #region DOM Event Handlers
	// #endregion

	// #region Computed Properties
	// #endregion

	// #region Private Methods
	// #endregion

	// #region Default Sub-components
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-masthead-container-title');
	// #endregion
}
