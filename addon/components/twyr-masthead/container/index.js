import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class TwyrMastheadContainerComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	@tracked _showCollapsedMenu = false;
	@tracked collapsedMenuElement = null;
	@tracked designSystem = null;

	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	willDestroy() {
		this.#debug(`willDestroy`);
		super.willDestroy(...arguments);
	}
	// #endregion

	// #region DOM Event Handlers
	@action
	onActionModeChanged(changedArgs) {
		this.#debug(`onActionModeChanged: `, changedArgs);
		if(this?.args?.actionMode) this._showCollapsedMenu = false;
	}

	@action
	onDesignSystemChange(designSystem) {
		this.#debug(`onDesignSystemChange::designSystem: ${designSystem}`);
		this.designSystem = designSystem;
	}

	@action
	storeElement(element) {
		this.#debug(`storeElement: `, element);
		this.#element = element;
	}

	@action
	storeCollapsedMenuElement(element) {
		this.#debug(`storeMenuElement: `, element);
		this.collapsedMenuElement = element;
	}

	@action
	toggleCollapsedMenu(state) {
		this.#debug(`toggleCollapsedMenu: ${state}`);
		this._showCollapsedMenu = state ?? !this?._showCollapsedMenu;
	}
	// #endregion

	// #region Computed Properties
	get actionBarBackground() {
		this.#debug(`actionBarBackground: ${this?.args?.actionBarBackground ?? 'bg-light text-dark'}`);
		return this?.args?.actionBarBackground ?? 'bg-light text-dark';
	}

	get isContainerFluid() {
		const isFluid = ((this?.designSystem === 'material') || (this?.args?.actionMode ?? false) || (this?.args?.position !== 'top')) ? true : (this?.args?.fluid ?? false);
		this.#debug(`isContainerFluid? ${isFluid}`);

		return isFluid;
	}

	get brandComponent() {
		return this?._getComputedSubcomponent?.('brand');
	}

	get navigationComponent() {
		return this?._getComputedSubcomponent?.('navigation');
	}

	get titleComponent() {
		return this?._getComputedSubcomponent?.('title');
	}

	get overflowMenuComponent() {
		return this?._getComputedSubcomponent?.('actions');
	}

	get showCollapsedMenu() {
		if(this?.args?.actionMode)
			return false;

		return this._showCollapsedMenu;
	}
	// #endregion

	// #region Private Methods
	_getComputedSubcomponent(componentName) {
		const subComponent = this?.args?.customComponents?.[componentName] ?? this.#subComponents?.[componentName];
		this.#debug(`${componentName}-component`, subComponent);

		return subComponent;
	}
	// #endregion

	// #region Default Sub-components
	#subComponents = {
		'brand': 'twyr-masthead/container/brand',
		'navigation': 'twyr-masthead/container/navigation',
		'title': 'twyr-masthead/container/title',
		'actions': 'twyr-masthead/container/actions'
	};
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-masthead-container');
	#element = null;
	// #endregion
}
