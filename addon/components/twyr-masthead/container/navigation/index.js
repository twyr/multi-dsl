import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class TwyrMastheadContainerNavigationComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	@tracked collapsed = false;
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);

		this.#measurerElement = document.createElement('canvas');
		this.#measurerContext = this.#measurerElement.getContext('2d');
	}
	// #endregion

	// #region Lifecycle Hooks
	// #endregion

	// #region DOM Event Handlers
	@action
	onResize(entry) {
		if(this?.args?.actionMode)
			return;

		if(!this.#element)
			return;

		const menuElement = this.#element?.querySelector?.('ul');
		this.#measurerContext.font = getComputedStyle?.(menuElement)?.getPropertyValue?.('font');

		const elementWidth = entry?.contentRect?.width;
		const menuWidth = this.#measurerContext?.measureText?.(menuElement?.outerHTML)?.width;

		const collapse = menuWidth > elementWidth;
		this.#debug?.(`onResize: Element Width: ${elementWidth}, Menu Width: ${menuWidth}, Min required width: ${this.#minElementWidth}, Collapse? ${collapse}, Current State: ${this.collapsed}`);

		if(this.collapsed === collapse)
			return;

		if(!this?.collapsed && collapse) {
			this.#minElementWidth = menuWidth;

			this.collapsed = true;
			this.toggleCollapsedMenu(!this.collapsed);
		}

		if(this?.collapsed && !collapse) {
			if(elementWidth < this.#minElementWidth)
				return;

			this.collapsed = false;
			this.toggleCollapsedMenu(!this.collapsed);
		}
	}

	@action
	storeElement(element) {
		this.#debug(`storeElement`);
		this.#element = element;
	}

	@action
	toggleCollapsedMenu(state) {
		this.#debug(`toggleCollapsedMenu: ${state}`);
		if(typeof state !== 'boolean')
			state = null;

		const toggleCollapsedMenuAction = this?.args?.toggleCollapsedMenu;
		toggleCollapsedMenuAction?.(state);
	}
	// #endregion

	// #region Computed Properties
	// #endregion

	// #region Private Methods
	// #endregion

	// #region Default Sub-components
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-masthead-container-navigation');
	#element = null;

	#measurerContext = null;
	#measurerElement = null;

	#minElementWidth = 0;
	// #endregion
}
