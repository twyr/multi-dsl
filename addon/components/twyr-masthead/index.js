import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

export default class TwyrMastheadComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	willDestroy() {
		this.#debug(`willDestroy`);
		super.willDestroy(...arguments);
	}
	// #endregion

	// #region DOM Event Handlers
	// #endregion

	// #region Computed Properties
	get mastheadPosition() {
		return this?.args?.position ?? 'top';
	}

	get containerComponent() {
		return this?._getComputedSubcomponent?.('container');
	}
	// #endregion

	// #region Private Methods
	_getComputedSubcomponent(componentName) {
		const subComponent = this?.args?.customComponents?.[componentName] ?? this.#subComponents?.[componentName];
		this.#debug(`${componentName}-component`, subComponent);

		return subComponent;
	}
	// #endregion

	// #region Default Sub-components
	#subComponents = {
		'container': 'twyr-masthead/container'
	};
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-masthead');
	// #endregion
}
