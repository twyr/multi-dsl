import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { run } from '@ember/runloop';
import { tracked } from '@glimmer/tracking';

export default class TwyrSidebarComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	@tracked isOpen = false;
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);

		this.#controls.open = this?._open;
		this.#controls.close = this?._close;
		this.#controls.toggle = this?._toggle;
		this.#controls.name = this?.args?.name ?? 'sidebar';
	}
	// #endregion

	// #region Lifecycle Hooks
	willDestroy() {
		this.#debug(`willDestroy`);

		if(this?.args?.controlElement) {
			this.#debug(`willDestroy: unregistering with control`);
			this?.args?.controlElement?.sidebarControls?.unregisterSidebar?.(this.#element);
		}

		super.willDestroy(...arguments);
	}
	// #endregion

	// #region DOM Event Handlers
	@action
	onClickOutside(event) {
		this.#debug(`onClickOutside: `, event);
		if(!this?.clickOutsideToClose) {
			this.#debug(`onClickOutside::clickOutsideToClose: ${this?.clickOutsideToClose}`);
			return;
		}

		const isEventOnControl = ((event.target === this?.args?.controlElement) || this?.args?.controlElement.contains(event.target));
		if(isEventOnControl) {
			this.#debug(`onClickOutside::isEventOnControl: ${isEventOnControl}`);
			return;
		}

		this?._close?.();
	}

	@action
	storeElement(element) {
		this.#debug(`storeElement: `, element);
		this.#element = element;

		if(!this?.isOpen && !this?.locked) {
			if(this?.position === 'left' || this?.position === 'right')
				this.#element?.classList?.add?.('max-w-0', 'hidden');

			if(this?.position === 'bottom')
				this.#element?.classList?.add?.('max-h-0', 'hidden');
		}

		if(!this?.args?.controlElement) {
			this.#debug(`storeElement: control element not specified. aborting...`);
			return;
		}

		this?.args?.controlElement?.sidebarControls?.registerSidebar?.(this.#element, this.#controls);
	}

	@action
	registerWithControl() {
		this.#debug(`registerWithControl`);

		if(!this.#element) {
			this.#debug(`registerWithControl: element not in dom. aborting...`);
			return;
		}

		this?.args?.controlElement?.sidebarControls?.registerSidebar?.(this.#element, this.#controls);
	}
	// #endregion

	// #region Computed Properties
	get clickOutsideToClose() {
		return this?.args?.clickOutsideToClose ?? true;
	}

	get locked() {
		return this?.args?.locked ?? false;
	}

	get modal() {
		return this?.args?.modal ?? true;
	}

	get overlay() {
		return this?.args?.overlay ?? true;
	}

	get position() {
		return this?.args?.position?.trim?.() ?? 'left';
	}
	// #endregion

	// #region Private Methods
	@action
	_open() {
		this.#debug(`_open`);

		this.#element?.classList?.remove?.('max-w-0', 'max-h-0', 'hidden');
		this.isOpen = true;
	}

	@action
	_close() {
		this.#debug(`_close`);

		if(this?.locked) {
			this.#debug(`_close: sidebar is locked. aborting...`);
			return;
		}

		const elementStyle = window?.getComputedStyle?.(this.#element);
		let transitionDuration = Number(elementStyle?.getPropertyValue?.('--transition-interval')?.replace('ms', '')?.trim?.() ?? '0');
		if(!transitionDuration) transitionDuration = 300;

		this.isOpen = false;
		run?.later?.(this, () => {
			if(this?.position !== 'bottom')
				this.#element?.classList?.add?.('max-w-0', 'hidden');
			else
				this.#element?.classList?.add?.('max-h-0', 'hidden');
		}, {}, (transitionDuration * 1.1));
	}

	@action
	_toggle() {
		this.#debug(`_toggle`);
		if(this?.isOpen)
			this?._close?.();
		else
			this?._open?.();
	}
	// #endregion

	// #region Default Sub-components
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-sidebar');
	#element = null;

	#controls = {};
	// #endregion
}
