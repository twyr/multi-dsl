import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { run } from '@ember/runloop';

export default class TwyrBannerContainerComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	// #endregion

	// #region DOM Event Handlers
	@action
	storeElement(element) {
		this.#debug(`storeElement: `, element);
		this.#element = element;

		run.later(this, this?.positionCalculator, {}, 2000);
	}
	// #endregion

	// #region Computed Properties
	get positionCalculator() {
		return this?.args?.bannerPositionCalculator ?? this._calcBannerPosition;
	}
	// #endregion

	// #region Private Methods
	@action
	_calcBannerPosition() {
		const parentElement = this.#element?.parentNode;
		if(!parentElement) {
			this.#element?.classList?.add?.('top-0');
			return;
		}

		const otherStickies = parentElement?.querySelectorAll?.(`nav.twyr-masthead.top`);
		if(!otherStickies.length) {
			this.#element?.classList?.add?.('top-0');
			return;
		}
		let heightAlreadyUsed = 0;
		otherStickies.forEach((otherSticky) => {
			const mastheadStyle = window?.getComputedStyle?.(otherSticky);
			const mastheadHeight = mastheadStyle?.height?.replace('px', '');

			heightAlreadyUsed += Number(mastheadHeight);
		});

		if(heightAlreadyUsed === 0) {
			this.#element?.classList?.add?.('top-0');
			return;
		}

		this.#element.style.top = `${heightAlreadyUsed + 1}px`;
	}
	// #endregion

	// #region Default Sub-components
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-banner-container');
	#element = null;
	// #endregion
}
