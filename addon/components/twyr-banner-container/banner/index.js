import Component from '@glimmer/component';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
export default class TwyrBannerContainerBannerComponent extends Component {
	// #region Accessed Services
	// #endregion

	// #region Tracked Attributes
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	// #endregion

	// #region DOM Event Handlers
	@action
	storeElement(element) {
		this.#debug(`storeElement: `, element);
		this.#element = element;
	}
	// #endregion

	// #region Computed Properties
	get bannerContainerElement() {
		this.#debug(`bannerContainerElement`);
		if(!this.#element) {
			this.#debug(`bannerContainerElement: element uninitialized. returning first available in the document`);
			return document?.querySelector('div.twyr-banner-container');
		}

		let currentParent = this.#element;
		let bannerContainer = null;

		do {
			bannerContainer = currentParent.querySelector('div.twyr-banner-container');
			if(bannerContainer) break;

			currentParent = currentParent.parentNode;
		} while(currentParent.tagName.toLowerCase() !== 'body');

		if(bannerContainer) {
			this.#debug(`bannerContainerElement: `, bannerContainer);
			return bannerContainer;
		}

		this.#debug(`bannerContainerElement: none found in ancestors. returning first available in the document`);
		return document?.querySelector('div.twyr-banner-container');
	}

	get buttonComponent() {
		return this?._getComputedSubcomponent?.('button');
	}

	get iconComponent() {
		return this?._getComputedSubcomponent?.('icon');
	}
	// #endregion

	// #region Private Methods
	_getComputedSubcomponent(componentName) {
		const subComponent = this?.args?.customComponents?. [componentName] ?? this.#subComponents?. [componentName];
		this.#debug(`${componentName}-component`, subComponent);

		return subComponent;
	}
	// #endregion

	// #region Default Sub-components
	#subComponents = {
		'button': 'twyr-button',
		'icon': 'twyr-icon'
	};
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('component:twyr-banner-container-banner');
	#element = null;
	// #endregion
}
