import Modifier from 'ember-modifier';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { nextBrowserTick as nextTick } from './../utils/next-browser-tick';
import { run } from '@ember/runloop';
import { supportsPassiveEventListeners } from './../utils/check-browser-features';

export default class WillRippleModifier extends Modifier {
	// #region Accessed Services
	@service('colourUtilities') colourUtilities;
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	didInstall() {
		super.didInstall(...arguments);
		this.#debug(`didInstall: `, this.args);

		// Create the Ripple container
		this.#rippleContainer = document.createElement('div');
		this.#rippleContainer.classList.add('ripple-container');

		// Attach the Ripple container to the element
		this?.element?.appendChild?.(this.#rippleContainer);

		// Bind the DOM Event Handlers
		this?.element?.addEventListener?.('mousedown', this?.onMouseDown, true);
		this?.element?.addEventListener?.('mouseup', this?.onMouseUp, true);
		this?.element?.addEventListener?.('mouseleave', this?.onMouseUp, true);

		const options = supportsPassiveEventListeners ? { 'passive': true } : false;
		this?.element?.addEventListener?.('touchend', this?.onMouseUp, options);
		this?.element?.addEventListener?.('touchmove', this?.onTouchMove, options);
	}

	didReceiveArguments() {
		super.didReceiveArguments(...arguments);
		this.#debug(`didReceiveArguments:\nelement: `, this?.element, `\nargs: `, this?.args);

		if(!this.#ripples.length)
			return;

		this?._deleteRipples?.();
	}

	willDestroy() {
		this.#debug(`willDestroy`);

		const options = supportsPassiveEventListeners ? { 'passive': true } : false;
		this?.element?.removeEventListener?.('touchmove', this?.onTouchMove, options);
		this?.element?.removeEventListener?.('touchend', this?.onMouseUp, options);

		this?.element?.removeEventListener?.('mouseleave', this?.onMouseUp, true);
		this?.element?.removeEventListener?.('mouseup', this?.onMouseUp, true);
		this?.element?.removeEventListener?.('mousedown', this?.onMouseDown, true);

		super.willDestroy(...arguments);
	}
	// #endregion

	// #region DOM Event Handlers
	@action
	onMouseDown(event) {
		this.#debug('onMouseDown: ', event);
		if(this.#mouseDown) return;

		this.#mouseDown = true;
		if(event?.originalEvent) event = event?.originalEvent;

		if(event?.srcElement === this?.element) {
			this?._createRipple?.(event.offsetX, event.offsetY);
			return;
		}

		const layerRect = this?.element?.getBoundingClientRect?.();
		this?._createRipple?.((event?.clientX - layerRect?.left), (event?.clientY - layerRect?.top));
	}

	@action
	async onMouseUp(event) {
		this.#debug('onMouseUp: ', event);
		if(!(this.#mouseDown || this.#lastRipple))
			return;

		if(this.#timeout)
			return;

		await nextTick?.();
		this.#mouseDown = false;
		run?.later?.(this, () => {
			this?._deleteRipples?.();
		}, {}, 800);
	}

	@action
	async onTouchMove(event) {
		this.#debug('onTouchMove: ', event);
		if(!(this.#mouseDown || this.#lastRipple))
			return;

		this.#mouseDown = false;

		await nextTick?.();
		run?.later?.(this, () => {
			this?._deleteRipples?.();
		}, {}, 800);
	}
	// #endregion

	// #region Computed Properties
	get isRippleAllowed() {
		let element = this?.element;

		const elementStyle = getComputedStyle?.(element);
		const designSystem = elementStyle?.getPropertyValue?.('--twyr-design-system')?.trim?.();

		if((designSystem !== 'material') && (designSystem !== 'tailwind'))
			return false;

		do {
			if(!element?.tagName || (element?.tagName?.toUpperCase() === 'BODY'))
				break;

			if(element?.hasAttribute?.('disabled')) {
				this.#debug(`isRippleAllowed::element::disabled: false`);
				return false;
			}

			element = element?.parentNode;
		} while(element);

		this.#debug('isRippleAllowed: true');
		return true;
	}

	get rippleColour() {
		const rippleColour = this?.colourUtilities?.getOverlayColour(this.element);

		this.#debug('rippleColour: ', rippleColour ?? 'rgb(0, 0, 0)');
		return rippleColour ?? 'rgb(0, 0, 0)';
	}
	// #endregion

	// #region Private Methods
	async _createRipple(left, top) {
		this.#debug(`_createRipple::left: ${left}, top: ${top}`);
		if(!this.isRippleAllowed)
			return;

		// Basic Ripple element
		const ripple = document?.createElement?.('div');
		ripple?.classList?.add?.('ripple');

		// Calculate Ripple dimensions
		const width = this?.element?.clientWidth;
		const height = this?.element?.clientHeight;

		const x = Math.max(Math.abs(width - left), left) * 2;
		const y = Math.max(Math.abs(height - top), top) * 2;

		const size = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
		// const size = Math.max(x, y);
		const colour = this?.rippleColour;

		const rippleCss = `
			left: ${left}px;
			top: ${top}px;
			width: ${size}px;
			height: ${size}px;
			background-color: ${colour ? colour?.replace('rgba', 'rgb').replace(/,[^),]+\)/, ')') : 'rgb(0,0,0)'};
			border-color: ${colour ? colour?.replace('rgba', 'rgb').replace(/,[^),]+\)/, ')') : 'rgb(0,0,0)'};
			pointer-events: none;
		`;

		// Setup the Ripple
		ripple.style.cssText = rippleCss;
		this.#lastRipple = ripple;

		// Run the Ripple
		this.#debug(`_createRipple::ripple:`, ripple);

		this?._clearTimeout?.();
		this.#timeout = run?.later?.(this, () => {
			this?._clearTimeout?.();

			if(this.#mouseDown)
				return;

			this?._fadeInComplete?.(ripple);
		}, {}, 800);

		// Add it to the DOM, Collections, etc.
		this.#rippleContainer?.appendChild?.(ripple);
		this.#ripples?.push?.(ripple);
		ripple.classList.add('ripple-placed');

		// Finish it off
		await nextTick?.();
		ripple.classList.add('ripple-scaled', 'ripple-active');

		run?.later?.(this, () => {
			this?._deleteRipples?.();
		}, {}, 1000);
	}

	_clearRipples() {
		for(let idx = this.#ripples?.length; idx >= 0; idx--)
			this?._fadeInComplete?.(this.#ripples?. [idx]);
	}

	_deleteRipples() {
		for(let idx = this.#ripples?.length; idx >= 0; idx--) {
			const ripple = this.#ripples?.[idx];
			this?._removeRipple?.(ripple);
		}
	}

	_removeRipple(ripple) {
		if(!this.#ripples.includes(ripple))
			return;

		ripple.classList.remove('ripple-active', 'ripple-scaled');
		ripple.classList.add('ripple-remove');

		this.#ripples?.splice?.(this.#ripples?.indexOf?.(ripple), 1);
		run.later(this, () => {
			ripple?.parentNode?.removeChild?.(ripple);
			ripple?.remove?.();

			if(ripple === this.#lastRipple)
				this.#lastRipple = null;
		}, {}, 200);
	}

	_clearTimeout() {
		if(!this.#timeout)
			return;

		run.cancel(this.#timeout);
		this.#timeout = null;
	}

	_fadeInComplete(ripple) {
		if(this.#lastRipple !== ripple) {
			this?._removeRipple?.(ripple);
			return;
		}

		if(!this.#timeout && !this.#mouseDown) {
			this?._removeRipple?.(ripple);
			return;
		}
	}
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('modifier:will-ripple');
	#mouseDown = false;

	#rippleContainer = null;

	#ripples = [];
	#lastRipple = null;
	#timeout = null;
	// #endregion
}
