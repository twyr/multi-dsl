import Modifier from 'ember-modifier';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';

export default class CanReceiveFocusModifier extends Modifier {
	// #region Accessed Services
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	didInstall() {
		super.didInstall(...arguments);
		this.#debug(`didInstall: `, this.args);

		this?.element?.addEventListener?.('focusin', this?.onFocusIn, true);
		this?.element?.addEventListener?.('focusout', this?.onFocusOut, true);

		this?.element?.addEventListener?.('keydown', this?.onKeyDown, true);
		this?.element?.addEventListener?.('keyup', this?.onKeyUp, true);

		this?.element?.addEventListener?.('mouseenter', this?.onMouseEnter, true);
		this?.element?.addEventListener?.('mouseleave', this?.onMouseLeave, true);

		this?.element?.addEventListener?.('mousedown', this?.onMouseDown, true);
		this?.element?.addEventListener?.('mouseup', this?.onMouseUp, true);

		this?.element?.addEventListener?.('touchstart', this?.onTouchStart, true);
		this?.element?.addEventListener?.('touchend', this?.onTouchEnd, true);
		this?.element?.addEventListener?.('touchmove', this?.onTouchMove, true);
		this?.element?.addEventListener?.('touchcancel', this?.onTouchCancel, true);

		if(this?.element?.hasAttribute?.('tabindex'))
			return;

		const tabIndex = this?.element?.hasAttribute?.('disabled') ? '-1' : '0';
		this?.element?.setAttribute?.('tabindex', tabIndex);
	}

	didReceiveArguments() {
		super.didReceiveArguments(...arguments);
		this.#debug(`didReceiveArguments:\nelement: `, this?.element, `\nargs: `, this?.args);

		if(this?.element?.hasAttribute?.('tabindex'))
			return;

		const tabIndex = this?.element?.hasAttribute?.('disabled') ? '-1' : '0';
		this?.element?.setAttribute?.('tabindex', tabIndex);
	}

	willDestroy() {
		this.#debug(`willDestroy`);

		this?.element?.removeEventListener?.('touchcancel', this?.onTouchCancel, true);
		this?.element?.removeEventListener?.('touchmove', this?.onTouchMove, true);
		this?.element?.removeEventListener?.('touchend', this?.onTouchEnd, true);
		this?.element?.removeEventListener?.('touchstart', this?.onTouchStart, true);

		this?.element?.removeEventListener?.('mouseup', this?.onMouseUp, true);
		this?.element?.removeEventListener?.('mousedown', this?.onMouseDown, true);

		this?.element?.removeEventListener?.('mouseleave', this?.onMouseLeave, true);
		this?.element?.removeEventListener?.('mouseenter', this?.onMouseEnter, true);

		this?.element?.removeEventListener?.('keyup', this?.onKeyUp, true);
		this?.element?.removeEventListener?.('keydown', this?.onKeyDown, true);

		this?.element?.removeEventListener?.('focusout', this?.onFocusOut, true);
		this?.element?.removeEventListener?.('focusin', this?.onFocusIn, true);

		super.willDestroy(...arguments);
	}
	// #endregion

	// #region DOM Event Handlers
	@action
	onFocusIn(event) {
		if(this?.element?.hasAttribute?.('disabled') || (this?.args?.named?.focusOnlyOnKey === true))
			return;

		this.#debug('onFocusIn: ', event);
		if(this?.stopPropagation) event?.stopPropagation?.();

		if(event?.defaultPrevented)
			return;

		this?.element?.classList?.add?.(this?.focusClass);
		this?.args?.named?.onFocusIn?.(event);
	}

	@action
	onFocusOut(event) {
		if(this?.element?.hasAttribute?.('disabled'))
			return;

		this.#debug('onFocusOut: ', event);
		if(this?.stopPropagation) event?.stopPropagation?.();

		if(event?.defaultPrevented)
			return;

		this?.element?.classList?.remove?.(this?.focusClass);
		this?.args?.named?.onFocusOut?.(event);
	}

	@action
	onKeyDown(event) {
		if(this?.element?.hasAttribute?.('disabled'))
			return;

		this.#debug('onKeyDown: ', event);
		if(this?.stopPropagation) event?.stopPropagation?.();

		if(event?.defaultPrevented)
			return;

		this?.args?.named?.onKeyDown?.(event);
	}

	@action
	onKeyUp(event) {
		if(this?.element?.hasAttribute?.('disabled'))
			return;

		this.#debug('onKeyUp: ', event);
		if(this?.stopPropagation) event?.stopPropagation?.();

		if(event?.defaultPrevented)
			return;

		this?.args?.named?.onKeyUp?.(event);
	}

	@action
	onMouseEnter(event) {
		if(this?.element?.hasAttribute?.('disabled'))
			return;

		this.#debug('onMouseEnter: ', event);
		if(this?.stopPropagation) event?.stopPropagation?.();

		if(event?.defaultPrevented)
			return;

		this?.element?.classList?.add?.(this?.mouseOverClass);
		this?.args?.named?.onMouseEnter?.(event);
	}

	@action
	onMouseLeave(event) {
		if(this?.element?.hasAttribute?.('disabled'))
			return;

		this.#debug('onMouseLeave: ', event);
		if(this?.stopPropagation) event?.stopPropagation?.();

		if(event?.defaultPrevented)
			return;

		this?.element?.classList?.remove?.(this?.mouseOverClass);
		this?.args?.named?.onMouseLeave?.(event);
	}

	@action
	onMouseDown(event) {
		if(this?.element?.hasAttribute?.('disabled'))
			return;

		this.#debug('onMouseDown: ', event);
		if(this?.stopPropagation) event?.stopPropagation?.();

		if(this.#toggleBeingHandledByTouchEvents) {
			// Some devices have both touchscreen & mouse, and they are not mutually exclusive
			// In those cases, the touchdown handler is fired first, and it sets a flag to
			// short-circuit the mousedown so the component is not opened and immediately closed.
			this.#toggleBeingHandledByTouchEvents = false;
			return;
		}

		if(event?.defaultPrevented)
			return;

		this?.element?.classList?.add?.(this?.mouseDownClass);
		this?.args?.named?.onMouseDown?.(event);
	}

	@action
	onMouseUp(event) {
		if(this?.element?.hasAttribute?.('disabled'))
			return;

		this.#debug('onMouseUp: ', event);
		if(this?.stopPropagation) event?.stopPropagation?.();

		if(this.#toggleBeingHandledByTouchEvents) {
			// Some devices have both touchscreen & mouse, and they are not mutually exclusive
			// In those cases, the touchdown handler is fired first, and it sets a flag to
			// short-circuit the mouseup so the component is not opened and immediately closed.
			this.#toggleBeingHandledByTouchEvents = false;
			return;
		}

		if(event?.defaultPrevented)
			return;

		this?.element?.classList?.remove?.(this?.mouseDownClass);
		this?.args?.named?.onMouseUp?.(event);
	}

	@action
	onTouchStart(event) {
		if(this?.element?.hasAttribute?.('disabled'))
			return;

		this.#debug('onTouchStart: ', event);
		if(this?.stopPropagation) event?.stopPropagation?.();

		if(event?.defaultPrevented)
			return;

		this?.element?.classList?.add?.(this?.mouseDownClass);
		this?.args?.named?.onTouchStart?.(event);
	}

	@action
	onTouchEnd(event) {
		if(this?.element?.hasAttribute?.('disabled'))
			return;

		this.#debug('onTouchEnd: ', event);
		this.#toggleBeingHandledByTouchEvents = true;
		if(this?.stopPropagation) event?.stopPropagation?.();

		if(!event?.defaultPrevented) {
			this?.element?.classList?.remove?.(this?.mouseDownClass);
			this?.args?.named?.onTouchEnd?.(event);
		}

		// This next three lines are stolen from hammertime. This prevents the default
		// behaviour of the touchend, but synthetically trigger a focus and a (delayed) click
		// to simulate natural behaviour.
		event?.preventDefault?.();
		event?.target?.focus?.();

		setTimeout(() => {
			if(!event?.target) return;

			let evt;
			try {
				evt = document?.createEvent?.('MouseEvents');
				evt?.initMouseEvent?.('mouseup', true, true, window);
			}
			catch(err) {
				evt = new Event('mouseup');
			}
			finally {
				event?.target?.dispatchEvent?.(evt);
			}
		}, 0);
	}

	@action
	onTouchMove(event) {
		if(this?.element?.hasAttribute?.('disabled'))
			return;

		this.#debug('onTouchMove: ', event);
		if(this?.stopPropagation) event?.stopPropagation?.();

		if(event?.defaultPrevented)
			return;

		this?.args?.named?.onTouchMove?.(event);
	}

	@action
	onTouchCancel(event) {
		if(this?.element?.hasAttribute?.('disabled'))
			return;

		this.#debug('onTouchCancel: ', event);
		if(this?.stopPropagation) event?.stopPropagation?.();

		if(event?.defaultPrevented)
			return;

		this?.args?.named?.onTouchCancel?.(event);
	}
	// #endregion

	// #region Computed Properties
	get focusClass() {
		const focusClass = this?.args?.named?.focusClass ?? 'focused';

		this.#debug('focusClass: ', focusClass);
		return focusClass;
	}

	get mouseOverClass() {
		const mouseOverClass = this?.args?.named?.mouseOverClass ?? 'hovering';

		this.#debug('mouseOverClass: ', mouseOverClass);
		return mouseOverClass;
	}

	get mouseDownClass() {
		const mouseDownClass = this?.args?.named?.mouseDownClass ?? 'pressed';

		this.#debug('mouseDownClass: ', mouseDownClass);
		return mouseDownClass;
	}

	get stopPropagation() {
		return this?.args?.named?.stopPropagation ?? true;
	}
	// #endregion

	// #region Private Methods
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('modifier:can-receive-focus');
	#toggleBeingHandledByTouchEvents = false;
	// #endregion
}
