import Modifier from 'ember-modifier';
import debugLogger from 'ember-debug-logger';

import { inject as service } from '@ember/service';

export default class WatchDesignSystemChangeModifier extends Modifier {
	// #region Accessed Services
	@service('twyrOperatingParameters') operatingParameters;
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	didReceiveArguments() {
		super.didReceiveArguments(...arguments);
		this.#debug(`didReceiveArguments:\nelement: `, this?.element, `\nargs: `, this?.args);

		this?.operatingParameters?.watchDesignSystem?.(this?.element, this?.args?.positional?.[0]);
	}

	willDestroy() {
		this.#debug(`willDestroy`);

		this?.operatingParameters?.unwatchDesignSystem?.(this?.element);
		super.willDestroy(...arguments);
	}
	// #endregion

	// #region DOM Event Handlers
	// #endregion

	// #region Computed Properties
	// #endregion

	// #region Private Methods
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('modifier:watch-design-system-change');
	// #endregion
}
