import Modifier from 'ember-modifier';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { run } from '@ember/runloop';

export default class MaintainContrastModifier extends Modifier {
	// #region Accessed Services
	@service('colourUtilities') colourUtilities;
	@service('elementMutationWatcher') mutationWatcher;
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);
	}
	// #endregion

	// #region Lifecycle Hooks
	didInstall() {
		super.didInstall(...arguments);
		this.#debug(`didInstall: `, this.args);

		run?.later?.(this, () => {
			this?.mutationWatcher?.watchElement?.(this?.element, {
				'attributes': ['class', 'style']
			}, this?._recalcColours);
		}, {}, 250);
	}

	didReceiveArguments() {
		super.didReceiveArguments(...arguments);
		this.#debug(`didReceiveArguments:\nelement: `, this?.element, `\nargs: `, this?.args);

		run?.later?.(this, () => {
			const elementStyle = window?.getComputedStyle?.(this?.element);

			const sourceProp = this?.args?.named?.source ?? 'background-color';
			const sourceValue = elementStyle?.[sourceProp];

			const destProp = this?.args?.named?.dest ?? 'color';
			const destValue = elementStyle?.[destProp];

			this.#sourceValue = sourceValue;
			this.#destValue = destValue;
		}, {}, 100);
	}

	willDestroy() {
		this.#debug(`willDestroy`);
		this?.mutationWatcher?.unwatchElement?.(this?.element, this?._recalcColours);
	}
	// #endregion

	// #region DOM Event Handlers
	// #endregion

	// #region Computed Properties
	// #endregion

	// #region Private Methods
	@action
	_recalcColours() {
		const sourceProp = this?.args?.named?.source ?? 'background-color';
		const destProp = this?.args?.named?.dest ?? 'color';
		const elementStyle = window?.getComputedStyle?.(this?.element);

		const sourceValue = elementStyle?.[sourceProp];
		if(this.#sourceValue === sourceValue) {
			this.#debug(`_recalcColours:: source hasn't changed. aborting...`);

			this?.element?.style?.removeProperty?.(destProp);
			return;
		}

		const bwOnly = this?.args?.named?.bwOnly ?? false;
		const destValue = this?.colourUtilities?.getInverseColour?.(this?.element, null, sourceProp, bwOnly);

		if((this.#destValue === destValue)) {
			this.#debug(`_recalcColours:: dest hasn't changed. aborting...`);
			return;
		}

		if(this?.element?.style?.[destProp] === destValue) {
			this.#debug(`_recalcColours:: dest has already been applied. aborting...`);
			return;
		}

		this.#debug(`_recalcColours:: applying new ${destProp}: ${destValue}`);
		this.element.style[destProp] = destValue;
	}
	// #endregion

	// #region Private Attributes
	#debug = debugLogger('modifier:maintain-contrast');

	#destValue = null;
	#sourceValue = null;
	// #endregion
}
