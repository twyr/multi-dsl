import Controller from '@ember/controller';
import debugLogger from 'ember-debug-logger';

import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class ApplicationController extends Controller {
	// #region Accessed Services
	@service('alertManager') alertManager;
	// #endregion

	// #region Tracked Attributes
	@tracked actionMode = false;

	@tracked currentPage = 1;
	@tracked totalItems = 60;
	@tracked itemsPerPage = 30;

	@tracked xAlign = 'right';
	@tracked yAlign = 'bottom';

	@tracked xOffset = '0';
	@tracked yOffset = '0';

	@tracked equalTriggerContentWidth = false;

	@tracked divider = ``;

	@tracked sidebarControlElement = null;
	@tracked dialogVisible = false;
	// #endregion

	// #region Constructor
	constructor() {
		super(...arguments);
		this.#debug(`constructor`);

		// this.#interval = setInterval(this._toggleComponentArguments.bind(this), 250);
	}
	// #endregion

	// #region Lifecycle Hooks
	willDestroy() {
		this.#debug(`willDestroy`);

		if(this.#interval)
			clearInterval(this.#interval);


		super.willDestroy(...arguments);
	}
	// #endregion

	// #region DOM Event Handlers
	@action
	buttonClicked(event) {
		this.#debug('Button Clicked: ', event.target);
		['black', 'darkest', 'darker', 'dark', 'light', 'lighter', 'lightest', 'white', 'primary', 'secondary', 'tertiary', 'success', 'info', 'warning', 'error'].forEach((alertType) => {
			this?.alertManager?.notify({
				'type': alertType,
				'text': `Test Notification: ${alertType}`,
				'timeout': 3000,
				'xPosition': this.#xAlignments[this.#currentXAlign++],
				'yPosition': this.#yAlignments[this.#currentYAlign]
			});

			if(this.#currentXAlign === this.#xAlignments.length) {
				this.#currentXAlign = 0;
				this.#currentYAlign++;
			}

			if(this.#currentYAlign === this.#yAlignments.length)
				this.#currentYAlign = 0;
		});
	}

	@action
	closeDialog() {
		this.dialogVisible = false;
	}

	@action
	showDialog() {
		this.dialogVisible = true;
	}

	@action
	setSidebarControl(element) {
		this.sidebarControlElement = element;
	}

	@action
	onPageChange(pageNum) {
		this.currentPage = pageNum;
	}
	// #endregion

	// #region Computed Properties
	get dialogParent() {
		const modalContainerElement = document.getElementById('modal-parent');
		return modalContainerElement ?? document.body;
	}
	// #endregion

	// #region Private Methods
	_toggleComponentArguments() {
		this.actionMode = !this.actionMode;

		const currentXOffset = Number(this.xOffset);
		if(currentXOffset < 100) {
			this.xOffset = (currentXOffset + 1).toString();
		}
		else {
			this.xOffset = '0';

			let currentXAlignIndex = this.#xAlignments.indexOf(this.xAlign);
			if(currentXAlignIndex === (this.#xAlignments.length - 1)) currentXAlignIndex = -1;
			this.xAlign = this.#xAlignments[currentXAlignIndex + 1];
		}

		const currentYOffset = Number(this.yOffset);
		if(currentYOffset < 100) {
			this.yOffset = (currentYOffset + 1).toString();
		}
		else {
			this.yOffset = '0';

			let currentYAlignIndex = this.#yAlignments.indexOf(this.yAlign);
			if(currentYAlignIndex === (this.#yAlignments.length - 1)) currentYAlignIndex = -1;
			this.yAlign = this.#yAlignments[currentYAlignIndex + 1];
		}

		this.#debug(`xAlign: ${this.xAlign}, xOffset: ${this.xOffset}, yAlign: ${this.yAlign}, yOffset: ${this.yOffset}`);
	}
	// #endregion

	// #region Private Fields
	#debug = debugLogger('dummy-application-controller');
	#interval = null;

	#xAlignments = ['left', 'center', 'right'];
	#yAlignments = ['bottom', 'middle', 'top'];

	#currentXAlign = 0;
	#currentYAlign = 0;
	// #endregion
}
