import nextBrowserTick from 'dummy/utils/next-browser-tick';
import { module, test } from 'qunit';

module('Unit | Utility | next-browser-tick', function() {

  // TODO: Replace this with your real tests.
  test('it works', function(assert) {
    const result = nextBrowserTick();
    assert.ok(result);
  });
});
