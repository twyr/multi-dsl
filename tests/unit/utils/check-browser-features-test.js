import checkBrowserFeatures from 'dummy/utils/check-browser-features';
import { module, test } from 'qunit';

module('Unit | Utility | check-browser-features', function() {

  // TODO: Replace this with your real tests.
  test('it works', function(assert) {
    const result = checkBrowserFeatures();
    assert.ok(result);
  });
});
