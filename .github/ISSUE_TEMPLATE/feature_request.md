---
name: Feature Request
about: Suggest a *new* idea to improve this project
title: "[component_name]:"
labels: 'feature'
assignees: shadyvd

---

**Is your feature request related to a problem? Please describe.**
A clear and concise description of what the problem is. Ex. I'm always frustrated when [...]

**Describe the solution you'd like**
A clear and concise description of what you want to happen.

**Additional context**
Add any other context or screenshots about the feature request here.
